\documentclass{beamer}
\mode<presentation>{\usetheme{Warsaw}}
\usepackage[utf8]{inputenc}
\usepackage{ucs}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{tikz}

\title{Boomerang attack}
\author{Ana Margarita Rodríguez Cordero }
\date{03/06/2021}

\begin{document}
\newtheorem{defi}{Definition}[section]
\begin{frame}{}
\maketitle    
\end{frame}

\section{Introduction}
\begin{frame}{Toy Ciphers}
    Let's consider Cherry and Juggly two 5-rounds toy ciphers of block and key size of 16 bits working at the nibble level. Cherry is a SPN cipher and Juggly a Feistel Network.
    \begin{block}{Cherry round function}
    \begin{itemize}
        \item ARK layer: The current state $X^i = x^i_0x^i_1x^i_2x^i_3$ is xored with the round key $K_i$.
        \item $P$ layer: The state is shifted by one nibble to the right.
        \item $S$ layer: Every nibble of the stated passes through the S-box.
        \item URK: The key is updated shifting one nibble to the left.
    \end{itemize}
    \end{block}
\end{frame}

\begin{frame}{Toy Ciphers}
    \begin{block}{Juggly round function}
        \begin{itemize}
            \item The state is divided in a left and right part, each being of $2$ nibble size: $ S = L||R = x_0x_1||x_2x_3 $.
            \item $R_i = L_{i-1}$, $L_i = P(S(L_{i-1}\oplus R_i))\oplus R_{i-1}$.
            \item The key is updated shifting one nibble to the left, the round key $K_i$ consists in the two left-most nibbles of the current key state.
        \end{itemize}
    \end{block}
    This toy ciphers will be used in order to exemplify the behaviour of an automatized boomerang attack with truncated differential characteristics.
\end{frame}

\begin{frame}{To have into account}
    Cherry  operators: Permutation layer ($P$) and S-box layer ($S$).
    $$y^i_0y^i_1y^i_2y^i_3 \leftarrow P(x^i_0x^i_1x^i_2x^i_3) = x^i_3x^i_0x^i_1x^i_2 $$
    The $S$ operator is given by the Skinny S-box: $$[c,6,9,0,1,q,2,b,3,8,5,d,4,e,7,f]$$
    From $P$ we get the equality constrain $\Delta y^i_j = \Delta x^i__{(1+j)\% 4}$ and from  $S$ $ \Delta y^i_j = \Delta x^i_j $
    \begin{itemize}
        \item Finding differential characteristics is performed in the same way as in differential attacks, having into account that for the second part of the ciphers, we might not have the same differential probabilities.
    \end{itemize}
    
\end{frame}

\begin{frame}{Will the differential come back?}
    \begin{itemize}
        \item The two part of the cipher need to be connected in the right way.
        \item Not every differential trail will be suitable for finding a good distinguisher.
    \end{itemize}
    Solution: Boomerang Connectivity Table in a middle third cipher the classical decomposition $E= E_0 \circ E_1$ passes to $E= \Tilde{E}_0 \oplus E_m \oplus \Tilde{E}_1$, where $\Tilde{E}_0 \smallsetminus E_m$ and $\Tilde{E}_1 \smallsetminus E_m$.
    
\end{frame}

\section{Boomerang Connectivity Table}
\begin{frame}{Boomerang Connectivity Table - BCT}
    \begin{defi}{}
        Les $S$ be a permutation of $\mathbb{F}^n_2$, and $\Delta_i,\nabla_o\in \mathbb{F}_2^n$. The Boomerang Connectivity Table of S is given by a $2^n\times 2^n$ table, in which the entry for the position $(\Delta_i,\nabla_o)$ is given by $BCT(\Delta_i,\nabla_o)$:
        \begin{equation*}
            \# \{ x\in \mathbb{F}_2^n : S^{-1}(S(x) \oplus \nabla_o) \oplus S^{-1} (S(x\oplus\Delta_i)\oplus \nabla_o) = \Delta_i \}
        \end{equation*}
    \label{BCT}
    \end{defi}
    \begin{equation*}
        \begin{split}
            BCT(\Delta_i,\nabla_o) = \# \{ S(x) \oplus \nabla_o : S(x)\oplus S(x\oplus \Delta) = \delta, \forall \delta \}\\
            \cap  \{ S(x) : S(x)\oplus S(x\oplus \Delta) = \delta, \forall \delta \}
        \end{split}
    \end{equation*}
\end{frame}

\begin{frame}{Feistel Boomerang Connectivity Table - FBCT}
    As the same function is used for encryption and decryption, the above formula cannot be used for Feistel-like block ciphers.
    \begin{defi}
        Let $S$ be a function from $\mathbb{F}_2^n$ to $F_2^m$, and $\Delta_i,\nabla_o \in \mathbb{F}_2^n$. The FBCT of S is given by a $2^2\times 2^2$ table T, in which the entry for the position $(\Delta_i,\nabla_o)$ is given by $BCT(\Delta_i,\nabla_o)$:
        \begin{equation*}
            \# \{ x \in \mathbb{F}_2^n : S(x) \oplus S(x \oplus \Delta_i ) \oplus S(x\oplus \nabla_o) \oplus S(x\oplus \Delta_i \oplus \nabla_o) = 0 \}
        \end{equation*}
    \end{defi}
\end{frame}

\begin{frame}{Properties on BCT and FBCT}
\begin{itemize}
    \item $BCT(\Delta,0) = BCT(0,\Delta) = 2^n$
    \item $FBCT(\Delta_i,\nabla_o) = FBCT(\nabla_o,\Delta_i)$.
    \item The first column, the first row and the diagonal have entry values $2^n$.
    \item $FBCT(\Delta_i,\nabla_o) \equiv 0 mod 4$.
    \item $FBCT(\Delta_i,\nabla_o) = FBCT(\Delta_i,\Delta_i\oplus \nabla_o)$.
\end{itemize}
    
\end{frame}

\begin{frame}{Proposed solution}
    \begin{itemize}
        \item Divide the encryption cipher into two $E = E_0 \circ E_1$.
        \item $r_0$ and $r_1$ variable number of encryption round of $E_0$ and $E_1$ respectively.
        \item Find the best differential trails for $E_0$ and $E^{-1}_1$ making $r_0$ and $r_1$ variable.
        \item Define $E_m$ as the last round of $E_0$ and the first of $E_1$, picking the differential trails that allow the boomerang to come back.
    \end{itemize}
    To study:
    \begin{itemize}
        \item  How to track the Boomerang connectivity table when there is more than one S-box in $E_m$.
        \item Would we have a better performance in a larger number of rounds for $E_m$?
        \item When targeting $E_m$, how distinguish between the Feistel and Susbtitution Permutation Network?
    \end{itemize}
    
\end{frame}

\begin{frame}{References}
\bibliographystyle{bstbib.bib}
    
\end{frame}

\end{document}
