function [Matrix,Matrix_inv] = matrix_propagation()
permutacion = [32,7,30,15,2,13,22,9,28,3,4,1,26,5,24,11,16,23,14,31,18,29,6,25,
  12,19,20,17,10,21,8,27];
n = 32;
perm = zeros(n);
perm_inv = zeros(n);
permutacion_inv = [12, 5, 10, 11, 14, 23, 2, 31, 8, 29, 16, 25, 6, 19, 4, 17, 28, 21, 26, 27, 30, 7, 18, 15, 24, 13, 32, 9, 22, 3, 20, 1];
bd = zeros(n-1);
for i = 1:n-1
    perm(permutacion(i),i) = 1;
    perm_inv(permutacion_inv(i),i) = 1;
    bd(i) = mod(i,2);
end
perm(permutacion(n),n) = 1;
perm_inv(permutacion_inv(n),n) = 1;
d = ones(n,1);
subs_add = diag(d)+diag(bd,-1);
Matrix = perm*subs_add;
Matrix_inv = subs_add*perm_inv;
end