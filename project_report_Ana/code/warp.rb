#!ruby
##
# Implements WARP cipher

require_relative "../dag.rb"
require_relative "../blocks/input.rb"
require_relative "../blocks/xor.rb"
require_relative "../blocks/subcell.rb"
require_relative "../blocks/permutation.rb"

require_relative "../writers/graphviz.rb"

def sbox()
    return [
        0xc, 0xa, 0xd, 0x3, 0xe, 0xb, 0xf, 0x7, 0x8, 0x9, 0x1, 0x5, 0x0, 0x2, 0x4, 0x6
    ]
end

def rcon()
    return [
        [0x0, 0x0, 0x1, 0x3, 0x7, 0xf, 0xf, 0xf, 0xe, 0xd, 0xa, 0x5, 0xa, 0x5, 0xb, 0x6, 0xc, 0x9, 0x3, 0x6, 0xd, 0xb, 0x7, 0xe, 0xd, 0xb, 0x6, 0xd, 0xa, 0x4, 0x9, 0x2, 0x4, 0x9, 0x3, 0x7, 0xe, 0xc, 0x8, 0x1, 0x2],
        [0x4, 0xc, 0xc, 0xc, 0xc, 0xc, 0x8, 0x4, 0x8, 0x4, 0x8, 0x4, 0xc, 0x8, 0x0, 0x4, 0xc, 0x8, 0x4, 0xc, 0xc, 0x8, 0x4, 0xc, 0x8, 0x4, 0x8, 0x0, 0x4, 0x8, 0x0, 0x4, 0xc, 0xc, 0x8, 0x0, 0x0, 0x4, 0x8, 0x4, 0xc]
    ]
end

def shuffle_p()
    return [11, 4, 9, 10, 13, 22, 1, 30, 7, 28, 15, 24, 5, 18, 3, 16, 27, 20, 25, 26, 29, 6, 17, 14, 23, 12, 31, 8, 21, 2, 19, 0]
end

# last_round = 1 => performs nb_rounds omiting the permutation in the last one
def create_warp_dag(nb_rounds,last_round)
    # define dag and inputs
    dag = Dag.new([],[],[],[])
    x,_,_ = dag.register_block(*input_block("X", NIBBLE_RANGE, [1,32]))
    dag.set_plaintexts(x.flatten)
    k,_,_ = dag.register_block(*input_block("K", NIBBLE_RANGE, [1,32]))
    dag.set_keys(k.flatten)
    dag.set_inputs(x.flatten+k.flatten)
    a = x
    b = [[16.times.map{|i| k[0][i]}], [16.times.map{|i| k[0][i+16]}]]
    if last_round == 1
        rounds = nb_rounds-1
    else
        rounds = nb_rounds
    end
    rounds.times do |round_number|
        #Substitution Layer
        even_state = [16.times.map{|i| a[0][2*i]}]
        odd_state  = [16.times.map{|i| a[0][2*i+1]}]
        temp_sbox_state,_,_ = dag.register_block(*subcell_block("S_#{round_number}",even_state,sbox))
        #Add round key - constant
        rc,_,_ = dag.register_block(*input_block("RC_#{round_number}", NIBBLE_RANGE, [1,16], [
            [rcon[0][round_number], rcon[1][round_number], 0x0, 0x0,
                               0x0,                   0x0, 0x0, 0x0,
                               0x0,                   0x0, 0x0, 0x0,
                               0x0,                   0x0, 0x0, 0x0]
        ]))
        odd_state,_,_ = dag.register_block(*xor_block("ARK_#{round_number}",[temp_sbox_state,b[round_number%2],odd_state,rc]))
        # Permutation Layer
        a = [16.times.map{|i| [even_state[0][i], odd_state[0][i]]}]
        a = [a.flatten]
        a,_,_ = dag.register_block(*permutation_block("P_#{round_number}",a,shuffle_p))
    end
    if last_round == 1
        round_number = nb_rounds - 1
        #Substitution Layer
        even_state = [16.times.map{|i| a[0][2*i]}]
        odd_state  = [16.times.map{|i| a[0][2*i+1]}]
        temp_sbox_state,_,_ = dag.register_block(*subcell_block("S_#{round_number}",even_state,sbox))
        #Add round key - constant
        rc,_,_ = dag.register_block(*input_block("RC_#{round_number}", NIBBLE_RANGE, [1,16], [
            [rcon[0][round_number], rcon[1][round_number], 0x0, 0x0,
                               0x0,                   0x0, 0x0, 0x0,
                               0x0,                   0x0, 0x0, 0x0,
                               0x0,                   0x0, 0x0, 0x0]
        ]))
        odd_state,_,_ = dag.register_block(*xor_block("ARK_#{round_number}",[temp_sbox_state,b[round_number%2],odd_state,rc]))
        a = [16.times.map{|i| [even_state[0][i], odd_state[0][i]]}]
        a = [a.flatten]
    end
    dag.set_outputs(a.flatten)
    return dag
end