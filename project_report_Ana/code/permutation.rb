require_relative "../dag.rb"
require_relative "../operators.rb"


## defines a permutation block
# @param [String] name of the states defined in this block
# @param [Vec<Vec<State>>] 2D matrix of state
def permutation_block(name, input, permutation, inverted=false)
    raise "PermutationBlock: incorrect permutation size (input size:#{input.flatten.length}, permutation size:#{permutation.length}" unless input.flatten.length == permutation.length
    x_size = input[0].length
    operators = []
    table = input.each_index.map do |y|
        input[y].length.times.map do |x|
            State.new("#{name}_#{y}_#{x}", input[y][x].domain)
        end
    end
    flat_input = []
    flat_table = []
    input[0].each_index do |x|
        input.each_index do |y|
            flat_input.push(input[y][x])
            flat_table.push(table[y][x])
        end
    end
    if inverted
        flat_input.each_index do |i|
            operators.push(EqualityOperator.new(
                flat_input[i],
                flat_table[permutation[i]]
            ))
            operators.last.origin_block = name
        end
    else
        flat_input.each_index do |i|
            operators.push(EqualityOperator.new(
                flat_input[permutation[i]],
                flat_table[i]
            ))
            operators.last.origin_block = name
        end
    end
    return table, table.flatten, operators
end
