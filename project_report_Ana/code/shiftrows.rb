require_relative "../dag.rb"
require_relative "../operators.rb"


## defines a shiftrows block
# @param [String] name of the states defined in this block
# @param [Vec<Vec<State>>] 2D matrix of state
# @param [bool] shift_right (for AES, it should be false, for SKINNY, it should be true)
def shiftrows_block(name, input, shift_right)
    x_size = input[0].length
    shift = x_size.times.map{|i| i}
    # case specific for Rijndael!
    if x_size == 7 && input.length == 4
        puts "WARNING: should be only used in Rijndael!!!"
        shift = [0,1,2,4]
    end
    if x_size == 8 && input.length == 4
        puts "WARNING: should be only used in Rijndael!!!"
        shift = [0,1,3,4]
    end
    operators = []
    # puts "xsize:#{x_size}\tshift:#{shift}"
    table = input.each_index.map do |y|
        input[y].length.times.map do |x|
            state = State.new("#{name}_#{y}_#{x}", input[y][x].domain)
            v = (shift[y]+x) % x_size
            if shift_right
                v = (x_size+x-shift[y]) % x_size
            end
            operators.push(EqualityOperator.new(
                input[y][v], state
            ))
            operators.last.origin_block = name
            state
        end
    end
    return table, table.flatten, operators
end