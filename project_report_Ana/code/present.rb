#!/ruby
##
# Implements PRESENT 80 - 128

require_relative "../dag.rb"
require_relative "../operators.rb"
require_relative "../blocks/input.rb"
require_relative "../blocks/xor.rb"
require_relative "../blocks/mixcolumns.rb"
require_relative "../blocks/permutation.rb"
require_relative "../blocks/subcell.rb"
def sbox()
    return[
        0xC, 0x5, 0x6, 0xB, 0x9, 0x0, 0xA, 0xD, 0x3, 0xE, 0xF, 0x8, 0x4, 0x7, 0x1, 0x2
    ]
end
def pLayer()
    return[
         0, 4,  8, 12,  16, 20, 24, 28,  32, 36, 40, 44,  48, 52, 56, 60,
         1, 5,  9, 13,  17, 21, 25, 29,  33, 37, 41, 45,  49, 53, 57, 61,
         2, 6, 10, 14,  18, 22, 26, 30,  34, 38, 42, 46,  50, 54, 58, 62,
         3, 7, 11, 15,  19, 23, 27, 31,  35, 39, 43, 47,  51, 55, 59, 63
    ]
end
def rc()
    return[
        [0,0,0,0,1],[0,0,0,1,0],[0,0,0,1,1],
        [0,0,1,0,0],[0,0,1,0,1],[0,0,1,1,0],[0,0,1,1,1],
        [0,1,0,0,0],[0,1,0,0,1],[0,1,0,1,0],[0,1,0,1,1],
        [0,1,1,0,0],[0,1,1,0,1],[0,1,1,1,0],[0,1,1,1,1],
        [1,0,0,0,0],[1,0,0,0,1],[1,0,0,1,0],[1,0,0,1,1],
        [1,0,1,0,0],[1,0,1,0,1],[1,0,1,1,0],[1,0,1,1,1],
        [1,1,0,0,0],[1,1,0,0,1],[1,1,0,1,0],[1,1,0,1,1],
        [1,1,1,0,0],[1,1,1,0,1],[1,1,1,1,0],[1,1,1,1,1]
    ]
end
def create_present_dag(nb_rounds,k_size)
    dag = Dag.new([],[],[],[])
    size = k_size/4
    x,_,_ = dag.register_block(*input_block("X",NIBBLE_RANGE, [1,16]))
    k,_,_ = dag.register_block(*input_block("K", NIBBLE_RANGE, [1,size]))
    dag.set_inputs(x.flatten+k.flatten)
    initial_message_state = x
    initial_key_state = k
    if nb_rounds < 32
        rounds = nb_rounds
        post_white = 0
    elsif nb_rounds >= 32
        rounds = nb_rounds - 1
        post_white = 1
    else
        raise "Maximum number of rounds = 32, current:#{nb_rounds}"
    end
    rounds.times do |round_number|
        #Add round key
        round_key = [16.times.map{|i| initial_key_state[0][i]}]
        state_ark,_,_= dag.register_block(*xor_block("ARK_#{round_number}",[initial_message_state, round_key ]))
        #Substitution Box
        state_sbox,_,_ = dag.register_block(*subcell_block("S_#{round_number}",state_ark,sbox))
        #Bit permutation
        state_sbox_bits,_,_ = dag.register_block(*input_block("S_bits_#{round_number}",BOOL_RANGE,[1,64]))
        16.times do |i|
            op = NibbleToBit.new([state_sbox[0][i]], [
                state_sbox_bits[0][i*4+0], state_sbox_bits[0][i*4+1], state_sbox_bits[0][i*4+2], state_sbox_bits[0][i*4+3]
            ])
            dag.operators.push(op)
        end
        state_pLayer_bits,_,_ = dag.register_block(*permutation_block("P_bits_#{round_number}",state_sbox_bits,pLayer))
        state_pLayer,_,_ = dag.register_block(*input_block("P_#{round_number}",NIBBLE_RANGE, [1,16]))
        16.times do |i|
            op1 = BitToNibble.new([state_pLayer_bits[0][i*4+0], state_pLayer_bits[0][i*4+1], state_pLayer_bits[0][i*4+2], state_pLayer_bits[0][i*4+3]],[
                state_pLayer[0][i]
            ])
            dag.operators.push(op1)
        end
        initial_message_state = state_pLayer
        ##### KEY UPDATE ####
        state_key_bits,_,_ = dag.register_block(*input_block("K_bits_#{round_number}",BOOL_RANGE,[1,k_size]))
        size.times do |i|
            op = NibbleToBit.new([initial_key_state[0][i]], [
                state_key_bits[0][i*4+0], state_key_bits[0][i*4+1], state_key_bits[0][i*4+2], state_key_bits[0][i*4+3]
            ])
            dag.operators.push(op)
        end
        #shitf 61 to the left
        state_keyShift_bits = [(61..(k_size-1)).map{|i| state_key_bits[0][i]}, 61.times.map{|i| state_key_bits[0][i]}]
        state_keyShift_bits = [state_keyShift_bits.flatten]
        #bit_round_number
        round_constant_bits = [k_size.times.map{|i| 0}]
        if k_size == 80
            round_constant_bits[0][60] = rc[round_number][0]
            round_constant_bits[0][61] = rc[round_number][1]
            round_constant_bits[0][62] = rc[round_number][2]
            round_constant_bits[0][63] = rc[round_number][3]
            round_constant_bits[0][64] = rc[round_number][4]
        elsif k_size == 128
            round_constant_bits[0][61] = rc[round_number][0]
            round_constant_bits[0][62] = rc[round_number][1]
            round_constant_bits[0][63] = rc[round_number][2]
            round_constant_bits[0][64] = rc[round_number][3]
            round_constant_bits[0][65] = rc[round_number][4]
        else
            raise "Present cryptosystem: key size must be 80 or 128 bits (current:#{k_size})"
        end
        state_round_constant_bits,_,_ = dag.register_block(*input_block("C_bits_#{round_number}",BOOL_RANGE, [1,k_size], values=round_constant_bits))
        #xor with the round_number
        state_keyXor_bits,_,_ = dag.register_block(*xor_block("ARC_bits_#{round_number}",[state_keyShift_bits, state_round_constant_bits ]))
        #################
        state_keyXor,_,_ = dag.register_block(*input_block("ARC_#{round_number}",NIBBLE_RANGE,[1,size]))
        size.times do |i|
            op1 = BitToNibble.new([state_keyXor_bits[0][i*4+0], state_keyXor_bits[0][i*4+1], state_keyXor_bits[0][i*4+2], state_keyXor_bits[0][i*4+3]], [
                state_keyXor[0][i]
            ])
            dag.operators.push(op1)
        end
        state_keySbox,_,_ = dag.register_block(*subcell_block("S_#{round_number}",state_keyXor,sbox))
        if k_size == 80
            initial_key_state = [1.times.map{|i| state_keySbox[0][i]}, (1..(size-1)).map{|i| state_keyXor[0][i]}]
        elsif k_size == 128
            initial_key_state = [2.times.map{|i| state_keySbox[0][i]}, (2..(size-1)).map{|i| state_keyXor[0][i]}]
        else
            raise "Present cryptosystem: key size must be 80 or 128 bits (current:#{k_size})"
        end
        initial_key_state = [initial_key_state.flatten]
    end
    ciphertext = initial_message_state
    if post_white == 1
        round_key = [16.times.map{|i| initial_key_state[0][i]}]
        state_ark,_,_= dag.register_block(*xor_block("ARK_#{nb_rounds}",[initial_message_state, round_key ]))
        ciphertext = state_ark
    end
    dag.set_outputs(ciphertext.flatten)
    return dag
end