#!ruby
##
# Implements SKINNY cipher

# directed acyclic graph helper functions
require_relative "../dag.rb"

# generic parts of the cryptosystem
require_relative "../blocks/input.rb"
require_relative "../blocks/xor.rb"
require_relative "../blocks/shiftrows.rb"
require_relative "../blocks/mixcolumns.rb"
require_relative "../blocks/permutation.rb"
require_relative "../blocks/subcell.rb"
require_relative "../blocks/lfsr.rb"
require_relative "../blocks/conditional_xor.rb"

# DAG display helper
require_relative "../writers/graphviz.rb"


def sbox4()
    return [12,6,9,0,1,10,2,11,3,8,5,13,4,14,7,15]
end

def sbox8()
    return [0x65, 0x4c, 0x6a, 0x42, 0x4b, 0x63, 0x43, 0x6b, 0x55, 0x75, 0x5a, 0x7a, 0x53, 0x73, 0x5b, 0x7b,
            0x35, 0x8c, 0x3a, 0x81, 0x89, 0x33, 0x80, 0x3b, 0x95, 0x25, 0x98, 0x2a, 0x90, 0x23, 0x99, 0x2b,
            0xe5, 0xcc, 0xe8, 0xc1, 0xc9, 0xe0, 0xc0, 0xe9, 0xd5, 0xf5, 0xd8, 0xf8, 0xd0, 0xf0, 0xd9, 0xf9,
            0xa5, 0x1c, 0xa8, 0x12, 0x1b, 0xa0, 0x13, 0xa9, 0x05, 0xb5, 0x0a, 0xb8, 0x03, 0xb0, 0x0b, 0xb9,
            0x32, 0x88, 0x3c, 0x85, 0x8d, 0x34, 0x84, 0x3d, 0x91, 0x22, 0x9c, 0x2c, 0x94, 0x24, 0x9d, 0x2d,
            0x62, 0x4a, 0x6c, 0x45, 0x4d, 0x64, 0x44, 0x6d, 0x52, 0x72, 0x5c, 0x7c, 0x54, 0x74, 0x5d, 0x7d,
            0xa1, 0x1a, 0xac, 0x15, 0x1d, 0xa4, 0x14, 0xad, 0x02, 0xb1, 0x0c, 0xbc, 0x04, 0xb4, 0x0d, 0xbd,
            0xe1, 0xc8, 0xec, 0xc5, 0xcd, 0xe4, 0xc4, 0xed, 0xd1, 0xf1, 0xdc, 0xfc, 0xd4, 0xf4, 0xdd, 0xfd,
            0x36, 0x8e, 0x38, 0x82, 0x8b, 0x30, 0x83, 0x39, 0x96, 0x26, 0x9a, 0x28, 0x93, 0x20, 0x9b, 0x29,
            0x66, 0x4e, 0x68, 0x41, 0x49, 0x60, 0x40, 0x69, 0x56, 0x76, 0x58, 0x78, 0x50, 0x70, 0x59, 0x79,
            0xa6, 0x1e, 0xaa, 0x11, 0x19, 0xa3, 0x10, 0xab, 0x06, 0xb6, 0x08, 0xba, 0x00, 0xb3, 0x09, 0xbb,
            0xe6, 0xce, 0xea, 0xc2, 0xcb, 0xe3, 0xc3, 0xeb, 0xd6, 0xf6, 0xda, 0xfa, 0xd3, 0xf3, 0xdb, 0xfb,
            0x31, 0x8a, 0x3e, 0x86, 0x8f, 0x37, 0x87, 0x3f, 0x92, 0x21, 0x9e, 0x2e, 0x97, 0x27, 0x9f, 0x2f,
            0x61, 0x48, 0x6e, 0x46, 0x4f, 0x67, 0x47, 0x6f, 0x51, 0x71, 0x5e, 0x7e, 0x57, 0x77, 0x5f, 0x7f,
            0xa2, 0x18, 0xae, 0x16, 0x1f, 0xa7, 0x17, 0xaf, 0x01, 0xb2, 0x0e, 0xbe, 0x07, 0xb7, 0x0f, 0xbf,
            0xe2, 0xca, 0xee, 0xc6, 0xcf, 0xe7, 0xc7, 0xef, 0xd2, 0xf2, 0xde, 0xfe, 0xd7, 0xf7, 0xdf, 0xff]
end

def shift_p()
    return [0,1,2,3,7,4,5,6,10,11,8,9,13,14,15,12]
end

def tweakey_p()
    return [6,10,0,1,15,11,4,5,2,3,8,9,7,14,12,13]
end

def rc()
    return [
        0x01, 0x03, 0x07, 0x0F, 0x1F, 0x3E, 0x3D, 0x3B, 0x37, 0x2F,
        0x1E, 0x3C, 0x39, 0x33, 0x27, 0x0E, 0x1D, 0x3A, 0x35, 0x2B,
        0x16, 0x2C, 0x18, 0x30, 0x21, 0x02, 0x05, 0x0B, 0x17, 0x2E,
        0x1C, 0x38, 0x31, 0x23, 0x06, 0x0D, 0x1B, 0x36, 0x2D, 0x1A,
        0x34, 0x29, 0x12, 0x24, 0x08, 0x11, 0x22, 0x04, 0x09, 0x13,
        0x26, 0x0c, 0x19, 0x32, 0x25, 0x0a, 0x15, 0x2a, 0x14, 0x28,
        0x10, 0x20
    ]
end

def mixcolumns_matrix()
    return [
        [1,0,1,1],
	    [1,0,0,0],
	    [0,1,1,0],
	    [1,0,1,0]
    ]
end



##
# z=1 : TK1 (|x|=|k|)
# z=2 : TK2 (2|x|=|k|)
# z=3 : TK3 (3|x|=|k|)
# @param z [{0,1,2,3}] SK,TK1,TK2,TK3
# @param v [{64,128}] skinny version
# TODO finish 128
def create_skinny_dag(nb_rounds, z, v)
    # define the DAG and its inputs
    dag = Dag.new([],[],[],[])
    usable_part = nil
    skinny_state_range = NIBBLE_RANGE
    if v == 64
        skinny_state_range = NIBBLE_RANGE
    elsif v == 128
        skinny_state_range = BYTE_RANGE
    else
        raise "create_skinny_dag invalid v parameter (should be in {64,128}, currently #{v})"
    end
    non_usable_part = 4.times.map{|i| 4.times.map{|j| 0}}
    x,_,_ = dag.register_block(*input_block("X",   skinny_state_range, [4,4]))
    k1,_,_ = dag.register_block(*input_block("K1", skinny_state_range, [4,4], values=(z >= 1 ? usable_part : non_usable_part)))
    k2,_,_ = dag.register_block(*input_block("K2", skinny_state_range, [4,4], values=(z >= 2 ? usable_part : non_usable_part)))
    k3,_,_ = dag.register_block(*input_block("K3", skinny_state_range, [4,4], values=(z >= 3 ? usable_part : non_usable_part)))
    if z == 0
        dag.set_inputs(x.flatten)
    elsif z == 1
        dag.set_inputs(x.flatten+k1.flatten)
    elsif z == 2
        dag.set_inputs(x.flatten+k1.flatten+k2.flatten)
    elsif z == 3
        dag.set_inputs(x.flatten+k1.flatten+k2.flatten+k3.flatten)
    else
        raise "create_skinny_dag: invalid z parameter (should be in {0,1,2,3}, current value: #{z})"
    end
    # define lfsr params
    tk2_params = [[1],[2],[3],[0,1]]
    tk3_params = [[0,3],[0],[1],[2]]
    # define the dag internals
    a = x
    b1 = k1
    b2 = k2
    b3 = k3
    nb_rounds.times do |round_number|
        ####### define cryptosystem internals (X message)
        a,_,_ = dag.register_block(*subcell_block("S_#{round_number}", a, (v==64) ? sbox4 : sbox8))
        c,_,_ = dag.register_block(*input_block("C_#{round_number}", skinny_state_range, [4,4], [
            [rc[round_number] & 0xf     , 0, 0, 0],
            [(rc[round_number]>>4) & 0x3, 0, 0, 0],
            [0x2                        , 0, 0, 0],
            [0                          , 0, 0, 0]
        ]))
        a,_,_ = dag.register_block(*xor_block("AC_#{round_number}", [a,c]))
        # add ART
        a,_,_ = dag.register_block(*conditional_xor_block("ART_#{round_number}", a, [b1,b2,b3], [
            [1,1,1,1],
            [1,1,1,1],
            [0,0,0,0],
            [0,0,0,0]
        ]))
        ####### define the cryptosystem internals (K message)
        b1,_,_ = dag.register_block(*permutation_block("KP1_#{round_number}", b1, tweakey_p))
        b2,_,_ = dag.register_block(*permutation_block("KP2_#{round_number}", b2, tweakey_p))
        b2,_,_ = dag.register_block(*lfsr_block("L2_#{round_number}", b2, [
            [1,1,1,1],
            [1,1,1,1],
            [0,0,0,0],
            [0,0,0,0]
        ], tk2_params))
        b3,_,_ = dag.register_block(*permutation_block("KP3_#{round_number}", b3, tweakey_p))
        b3,_,_ = dag.register_block(*lfsr_block("L3_#{round_number}", b3, [
            [1,1,1,1],
            [1,1,1,1],
            [0,0,0,0],
            [0,0,0,0]
        ], tk3_params))
        # shift rows
        a,_,_ = dag.register_block(*shiftrows_block("SR_#{round_number}", a,true))
        # mix columns
        a,_,_ = dag.register_block(*mixcolumns_block("MC_#{round_number}", a, mixcolumns_matrix))
    end
    # set the DAG outputs
    dag.set_outputs(a.flatten)
    return dag
end


# def main
#     dag = create_skinny_dag(32)
#     puts "WRITING SKINNY DOT FILE..."
#     # writefile_graphviz(dag.states, dag.operators, dag.inputs, dag.outputs, "tmp/test_skinny.dot")
#     puts "DONE"
#     result = dag.run([ # X
#         [0,0,0,0],
#         [0,0,0,0],
#         [0,0,0,0],
#         [0,0,0,0]
#     ].flatten + [ # K
#         [0,0,0,0],
#         [0,0,0,0],
#         [0,0,0,0],
#         [0,0,0,0]
#     ].flatten)
#     p result
# end

# main


