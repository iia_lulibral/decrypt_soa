from gurobipy import *
import const
import sys
import numpy as np

def milpMinimizationWarp(NBrounds,NDrounds,NFrounds):

	model = Model('WarpStep1_Rounds_MILP')
	# variables
	xb = model.addVars(32, NBrounds+1, vtype=GRB.BINARY, name="state_backward") 
	x  = model.addVars(32, NDrounds, vtype=GRB.BINARY, name="state") 
	xf = model.addVars(32, NFrounds+1, vtype=GRB.BINARY, name="state_forward") 
	a = model.addVar(vtype=GRB.BINARY,name="a") 
    
	# remove the trivial solution with no active nibble in the initial state
	model.addConstr(quicksum(x[i, 0] for i in range(32) ) >= 1, "Non Null State") 

	# linear layer	
	# nibbles that are kept (even ones)
	model.addConstrs( x[(2*i),r] == x[(const.Pi_even[i]),(r+1)] for i in range(16) for r in range(NDrounds-1))  
	
	# nibbles that go through Sb and are xored (odd ones)	
	model.addConstrs( x[2*i,r] + x[2*i+1,r] >=  x[ const.Pi_odd[i] ,r+1] for i in range(16) for r in range(NDrounds-1) ) 
	model.addConstrs( x[2*i,r] + x[ const.Pi_odd[i] ,r+1] >=  x[2*i+1,r] for i in range(16) for r in range(NDrounds-1) ) 
	model.addConstrs( x[2*i+1,r] + x[ const.Pi_odd[i] ,r+1] >= x[2*i,r]  for i in range(16) for r in range(NDrounds-1) ) 
    
    # matrix backward
	model.addConstrs( xb[i,NBrounds] == x[i,0] for i in range(32) )
	model.addConstrs( xb[i,r-1] >=  const.Matrix_inv[i][j]*xb[j,r] for j in range(32) for i in range(32) for r in range(NBrounds,0,-1) )
	model.addConstrs( xb[i,r-1] <= quicksum( const.Matrix_inv[i][j]*xb[j,r] for j in range(32) ) for i in range(32) for r in range(NBrounds,0,-1) )
    
    # matrix forward
	model.addConstrs( xf[i,0] == x[i,NDrounds-1] for i in range(32) )
	model.addConstrs( xf[i,r+1] >= const.Matrix[i][j]*xf[j,r] for j in range(32) for i in range(32) for r in range(NFrounds) )
	model.addConstrs( xf[i,r+1] <= quicksum( const.Matrix[i][j]*xf[j,r] for j in range(32) ) for i in range(32) for r in range(NFrounds) )
    
	# Complexity
	model.addConstr(  quicksum(x[2*i,r] for i in range(16) for r in range(NDrounds))*2 <= 126 )
	model.addConstr(  quicksum(x[2*i,r] for i in range(16) for r in range(NDrounds))*2 + quicksum(xb[i,0] for i in range(32)) + quicksum(xf[i,NFrounds] for i in range(32)) <= 255 )
    
	#objective
	model.setObjective(quicksum(x[2*i,r] for i in range(16) for r in range(NDrounds))*2 + quicksum(xb[i,0] for i in range(32)) + quicksum(xf[i,NFrounds] for i in range(32)) , GRB.MINIMIZE) # only even state pos through SB

	#resolution
	model.optimize()
	print("xb = ")
	for i in range(32):
		print(str(round (xb[i, 0].Xn)),",")
	for j in range(NDrounds):
		print("x_",j,"= ")
		for i in range(32):
			print(str(round (x[i,j].Xn)),",")
	print("xf = ")
	for i in range(32):
		print(str(round (xf[i,NFrounds].Xn)),",")
	print("#nbrounds = ", NBrounds, "#rounds = ", NDrounds, "#nfrounds = ", NFrounds, "optimal obj = ", model.ObjBound)


######################################
def main():
	NBrounds = int(sys.argv[1])
	NDrounds = int(sys.argv[2])
	NFrounds = int(sys.argv[3])
	milpMinimizationWarp(NBrounds,NDrounds,NFrounds)

if __name__ == '__main__':
	main()

           
