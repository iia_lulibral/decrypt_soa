#!ruby
##
# Implements DEOXYS cipher

require_relative "../dag.rb"
require_relative "../blocks/input.rb"
require_relative "../blocks/xor.rb"
require_relative "../blocks/shiftrows.rb"
require_relative "../blocks/mixcolumns.rb"
require_relative "../blocks/subcell.rb"
require_relative "../blocks/permutation.rb"
require_relative "../blocks/lfsr.rb"

require_relative "../writers/graphviz.rb"

def sbox()
    return [
        0x63, 0x7C, 0x77, 0x7B, 0xF2, 0x6B, 0x6F, 0xC5, 0x30, 0x01, 0x67, 0x2B, 0xFE, 0xD7, 0xAB, 0x76,
        0xCA, 0x82, 0xC9, 0x7D, 0xFA, 0x59, 0x47, 0xF0, 0xAD, 0xD4, 0xA2, 0xAF, 0x9C, 0xA4, 0x72, 0xC0,
        0xB7, 0xFD, 0x93, 0x26, 0x36, 0x3F, 0xF7, 0xCC, 0x34, 0xA5, 0xE5, 0xF1, 0x71, 0xD8, 0x31, 0x15,
        0x04, 0xC7, 0x23, 0xC3, 0x18, 0x96, 0x05, 0x9A, 0x07, 0x12, 0x80, 0xE2, 0xEB, 0x27, 0xB2, 0x75,
        0x09, 0x83, 0x2C, 0x1A, 0x1B, 0x6E, 0x5A, 0xA0, 0x52, 0x3B, 0xD6, 0xB3, 0x29, 0xE3, 0x2F, 0x84,
        0x53, 0xD1, 0x00, 0xED, 0x20, 0xFC, 0xB1, 0x5B, 0x6A, 0xCB, 0xBE, 0x39, 0x4A, 0x4C, 0x58, 0xCF,
        0xD0, 0xEF, 0xAA, 0xFB, 0x43, 0x4D, 0x33, 0x85, 0x45, 0xF9, 0x02, 0x7F, 0x50, 0x3C, 0x9F, 0xA8,
        0x51, 0xA3, 0x40, 0x8F, 0x92, 0x9D, 0x38, 0xF5, 0xBC, 0xB6, 0xDA, 0x21, 0x10, 0xFF, 0xF3, 0xD2,
        0xCD, 0x0C, 0x13, 0xEC, 0x5F, 0x97, 0x44, 0x17, 0xC4, 0xA7, 0x7E, 0x3D, 0x64, 0x5D, 0x19, 0x73,
        0x60, 0x81, 0x4F, 0xDC, 0x22, 0x2A, 0x90, 0x88, 0x46, 0xEE, 0xB8, 0x14, 0xDE, 0x5E, 0x0B, 0xDB,
        0xE0, 0x32, 0x3A, 0x0A, 0x49, 0x06, 0x24, 0x5C, 0xC2, 0xD3, 0xAC, 0x62, 0x91, 0x95, 0xE4, 0x79,
        0xE7, 0xC8, 0x37, 0x6D, 0x8D, 0xD5, 0x4E, 0xA9, 0x6C, 0x56, 0xF4, 0xEA, 0x65, 0x7A, 0xAE, 0x08,
        0xBA, 0x78, 0x25, 0x2E, 0x1C, 0xA6, 0xB4, 0xC6, 0xE8, 0xDD, 0x74, 0x1F, 0x4B, 0xBD, 0x8B, 0x8A,
        0x70, 0x3E, 0xB5, 0x66, 0x48, 0x03, 0xF6, 0x0E, 0x61, 0x35, 0x57, 0xB9, 0x86, 0xC1, 0x1D, 0x9E,
        0xE1, 0xF8, 0x98, 0x11, 0x69, 0xD9, 0x8E, 0x94, 0x9B, 0x1E, 0x87, 0xE9, 0xCE, 0x55, 0x28, 0xDF,
        0x8C, 0xA1, 0x89, 0x0D, 0xBF, 0xE6, 0x42, 0x68, 0x41, 0x99, 0x2D, 0x0F, 0xB0, 0x54, 0xBB, 0x16
    ]
end

def mixcolumns_matrix()
    return [
        [0x2,0x3,0x1,0x1],
        [0x1,0x2,0x3,0x1],
        [0x1,0x1,0x2,0x3],
        [0x3,0x1,0x1,0x2]
    ]
end

def rcon()
    return [
        0x2f, 0x5e, 0xbc, 0x63, 0xc6, 0x97, 0x35, 0x6a,
        0xd4, 0xb3, 0x7d, 0xfa, 0xef, 0xc5, 0x91, 0x39,
        0x72
    ]
end

def tweakey_p()
    return [4, 5, 6, 7, 9, 10, 11, 8, 14, 15, 12, 13, 3, 0, 1, 2]
end
def trans_p()
    return [0, 4, 8, 12, 1, 5, 9, 13, 2, 6, 10, 14, 3, 7, 11, 15]
end
# z = tweakey size with value 256 or 384
# k = 0: single key
# k = 1: TK1
# k = 2: TK2
# k = 3: TK3
def create_deoxys_dag(nb_rounds,z,k)
    unusable_part = 4.times.map{|i| 4.times.map{|j| 0}}
    usable_part = nil
    # define dag and inputs
    dag = Dag.new([],[],[],[])
    x,_,_ = dag.register_block(*input_block("X", BYTE_RANGE, [4,4]))
    dag.set_plaintexts(x.flatten)
    if z == 256
        k2,_,_ = dag.register_block(*input_block("TK1", BYTE_RANGE, [4,4], values=(k >= 1 ? usable_part : unusable_part)))
        k1,_,_ = dag.register_block(*input_block("TK2", BYTE_RANGE, [4,4], values=(k >= 2 ? usable_part : unusable_part)))
        dag.set_keys(k1.flatten+k2.flatten)
        dag.set_inputs(x.flatten+k2.flatten+k1.flatten)
    elsif z == 384
        k3,_,_ = dag.register_block(*input_block("TK1", BYTE_RANGE, [4,4], values=(k >= 1 ? usable_part : unusable_part)))
        k2,_,_ = dag.register_block(*input_block("TK2", BYTE_RANGE, [4,4], values=(k >= 2 ? usable_part : unusable_part)))
        k1,_,_ = dag.register_block(*input_block("TK3", BYTE_RANGE, [4,4], values=(k >= 3 ? usable_part : unusable_part)))
        dag.set_inputs(x.flatten+k3.flatten+k2.flatten+k1.flatten)
        dag.set_keys(k1.flatten+k2.flatten+k3.flatten)
        b3 = k3
        tk3_params = [[7,1],[0],[1],[2],[3],[4],[5],[6]]
    else
        raise "create_deoxys_dag: invalid z parameter (sould be in {256,384}, current value: #{z})"
    end
    a = x
    b1 = k1
    b2 = k2
    tk2_params = [[1],[2],[3],[4],[5],[6],[7],[0,2]]
    nb_rounds.times do |round_number|
        ####### define cryptosystem internals (X message)
        rc,_,_ = dag.register_block(*input_block("RC_#{round_number}", BYTE_RANGE, [4,4], [
            [0x01, 0x02, 0x04, 0x08],    
            [rcon[round_number], rcon[round_number], rcon[round_number], rcon[round_number]],
            [0x00, 0x00, 0x00, 0x00],
            [0x00, 0x00, 0x00, 0x00]
        ]))
        # tweakey xor
        if z == 256
            a,_,_ = dag.register_block(*xor_block("ART_#{round_number}", [a,b1,b2,rc]))
        elsif z == 384
            a,_,_ = dag.register_block(*xor_block("ART_#{round_number}", [a,b1,b2,b3,rc])) 
        else
            raise "create_deoxys_dag: invalid z parameter (sould be in {256,384}, current value: #{z})"
        end
        #substitution S8()
        a,_,_ = dag.register_block(*subcell_block("S_#{round_number}", a, sbox))
        ##transpose matrix
        a,_,_ = dag.register_block(*permutation_block("TRa_#{round_number}", a,trans_p))
        #ShiftRows
        a,_,_ = dag.register_block(*shiftrows_block("SR_#{round_number}", a,false))
        #MixBytes
        a,_,_ = dag.register_block(*mixcolumns_block("MC_#{round_number}", a, mixcolumns_matrix))
        ##transpose matrix
        a,_,_ = dag.register_block(*permutation_block("TRb_#{round_number}", a,trans_p))

        ########### key update
        b1,_,_ = dag.register_block(*permutation_block("KPa_#{round_number}", b1, tweakey_p))
        b2,_,_ = dag.register_block(*lfsr_block("La_#{round_number}", b2, [
            [1,1,1,1],
            [1,1,1,1],
            [1,1,1,1],
            [1,1,1,1]
        ], tk2_params))
        b2,_,_ = dag.register_block(*permutation_block("KPb_#{round_number}", b2, tweakey_p))
        if z == 384
            b3,_,_ = dag.register_block(*lfsr_block("Lb_#{round_number}", b3, [
                [1,1,1,1],
                [1,1,1,1],
                [1,1,1,1],
                [1,1,1,1]
            ], tk3_params))
            b3,_,_ = dag.register_block(*permutation_block("KP_#{round_number}", b3, tweakey_p))
        end
    end
    rc,_,_ = dag.register_block(*input_block("RC_#{nb_rounds}", BYTE_RANGE, [4,4], [
            [0x01, 0x02, 0x04, 0x08],    
            [rcon[nb_rounds], rcon[nb_rounds], rcon[nb_rounds], rcon[nb_rounds]],
            [0x00, 0x00, 0x00, 0x00],
            [0x00, 0x00, 0x00, 0x00]
        ]))
    # tweakey xor
    if z == 256
        a,_,_ = dag.register_block(*xor_block("ART_#{nb_rounds}", [a,b1,b2,rc]))
    elsif z == 384
        a,_,_ = dag.register_block(*xor_block("ART_#{nb_rounds}", [a,b1,b2,b3,rc]))
    else
        raise "create_deoxys_dag: invalid z parameter (sould be in {256,384}, current value: #{z})"
    end
    dag.set_outputs(a.flatten)
    return dag
end

def main
    dag = create_deoxys_dag(3, 384, 3)
    puts "WRITING DEOXYS DOT FILE..."
    writefile_graphviz(dag.states, dag.operators, dag.inputs, dag.outputs, "tmp/test_deoxys.dot")
    puts "DONE"
    result = dag.run([ # X
        [0,0,0,0],
        [0,0,0,0],
        [0,0,0,0],
        [0,0,0,0]
    ].flatten + [ # K1
        [0,0,0,0],
        [0,0,0,0],
        [0,0,0,0],
        [0,0,0,0]
    ].flatten + [ # K2
        [0,0,0,0],
        [0,0,0,0],
        [0,0,0,0],
        [0,0,0,0]
    ].flatten + [ # K3
        [0,0,0,0],
        [0,0,0,0],
        [0,0,0,0],
        [0,0,0,0]
    ].flatten)
    p result
end

main