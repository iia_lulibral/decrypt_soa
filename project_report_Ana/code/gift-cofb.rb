#!/ruby
##
# Implements GIFT-COFB

require_relative "../dag.rb"
require_relative "../operators.rb"
require_relative "../blocks/input.rb"
require_relative "../blocks/xor.rb"
require_relative "../blocks/and.rb"
require_relative "../blocks/or.rb"
require_relative "../blocks/not.rb"
require_relative "../blocks/permutation.rb"
def pLayer()
    return[
        [ 0, 4,  8, 12, 16, 20, 24, 28, 3, 7, 11, 15, 19, 23, 27, 31, 2, 6, 10, 14, 18, 22, 26, 30, 1, 5,  9, 13, 17, 21, 25, 29 ],
        [ 1, 5,  9, 13, 17, 21, 25, 29, 0, 4,  8, 12, 16, 20, 24, 28, 3, 7, 11, 15, 19, 23, 27, 31, 2, 6, 10, 14, 18, 22, 26, 30 ],
        [ 2, 6, 10, 14, 18, 22, 26, 30, 1, 5,  9, 13, 17, 21, 25, 29, 0, 4,  8, 12, 16, 20, 24, 28, 3, 7, 11, 15, 19, 23, 27, 31 ],
        [ 3, 7, 11, 15, 19, 23, 27, 31, 2, 6, 10, 14, 18, 22, 26, 30, 1, 5,  9, 13, 17, 21, 25, 29, 0, 4,  8, 12, 16, 20, 24, 28 ],
    ]
end
def pShiftKey
    return[
        110, 111,  96,  97,  98,  99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109,
        116, 117, 118, 119, 120, 121, 122, 123, 124, 125, 126, 127, 112, 113, 114, 115,
          0,   1,   2,   3,   4,   5,   6,   7,   8,   9,  10,  11,  12,  13,  14,  15,
         16,  17,  18,  19,  20,  21,  22,  23,  24,  25,  26,  27,  28,  29,  30,  31,  
         32,  33,  34,  35,  36,  37,  38,  39,  40,  41,  42,  43,  44,  45,  46,  47,
         48,  49,  50,  51,  52,  53,  54,  55,  56,  57,  58,  59,  60,  61,  62,  63,
         64,  65,  66,  67,  68,  69,  70,  71,  72,  73,  74,  75,  76,  77,  78,  79,
         80,  81,  82,  83,  84,  85,  86,  87,  88,  89,  90,  91,  92,  93,  94,  95
    ]
end
def rc()
     return[
        0x01, 0x03, 0x07, 0x0f, 0x1f, 0x3e, 0x3d,
        0x3b, 0x37, 0x2f, 0x1e, 0x3c, 0x39, 0x33,
        0x27, 0x0e, 0x1d, 0x3a, 0x35, 0x2b, 0x16,
        0x2c, 0x18, 0x30, 0x21, 0x02, 0x05, 0x0b,
        0x17, 0x2e, 0x1c, 0x38, 0x31, 0x23, 0x06,
        0x0d, 0x1b, 0x36, 0x2d, 0x1a, 0x34, 0x29,
        0x12, 0x24, 0x08, 0x11, 0x22, 0x04
    ]
end
def SubsBox(round_number,dag,s0,s1,s2,s3)
    temp,_,_ = dag.register_block(*and_block("SB_s1_#{round_number}",[s0,s2]))
    s1,_,_ = dag.register_block(*xor_block("SB_s1_#{round_number}",[s1,temp]))
    temp,_,_ = dag.register_block(*and_block("SB_s0_#{round_number}",[s1,s3]))
    s0,_,_ = dag.register_block(*xor_block("SB_s0_#{round_number}",[s0,temp]))
    temp,_,_ = dag.register_block(*or_block("SB_s2_#{round_number}",[s0,s1]))
    s2,_,_ = dag.register_block(*xor_block("SB_s2_#{round_number}",[s2,temp]))
    s3,_,_ = dag.register_block(*xor_block("SB_s3_#{round_number}",[s3,s2]))
    s1,_,_ = dag.register_block(*xor_block("SB_s1_#{round_number}",[s1,s3]))
    s3,_,_ = dag.register_block(*not_block("SB_s3_#{round_number}",s3))
    temp,_,_ = dag.register_block(*and_block("SB_s2_#{round_number}",[s0,s1]))
    s2,_,_ = dag.register_block(*xor_block("SB_s2_#{round_number}",[s2,temp]))
    return s0, s1, s2, s3
end
def PermBits(dag,round_number,s0,s1,s2,s3)
    ss0,_,_ = dag.register_block(*input_block("S0_bits_#{round_number}", BOOL_RANGE, [1,32]))
    ss1,_,_ = dag.register_block(*input_block("S1_bits_#{round_number}", BOOL_RANGE, [1,32]))
    ss2,_,_ = dag.register_block(*input_block("S2_bits_#{round_number}", BOOL_RANGE, [1,32]))
    ss3,_,_ = dag.register_block(*input_block("S3_bits_#{round_number}", BOOL_RANGE, [1,32]))
    4.times do |i|
        op0 = RangeTransform.new([s0[0][i]],[
            ss0[0][8*i + 0], ss0[0][8*i + 1], ss0[0][8*i + 2], ss0[0][8*i + 3], ss0[0][8*i + 4], ss0[0][8*i + 5], ss0[0][8*i + 6], ss0[0][8*i + 7]
        ])
        op1 = RangeTransform.new([s1[0][i]],[
            ss1[0][8*i + 0], ss1[0][8*i + 1], ss1[0][8*i + 2], ss1[0][8*i + 3], ss1[0][8*i + 4], ss1[0][8*i + 5], ss1[0][8*i + 6], ss1[0][8*i + 7]
        ])
        op2 = RangeTransform.new([s2[0][i]],[
            ss2[0][8*i + 0], ss2[0][8*i + 1], ss2[0][8*i + 2], ss2[0][8*i + 3], ss2[0][8*i + 4], ss2[0][8*i + 5], ss2[0][8*i + 6], ss2[0][8*i + 7]
        ])
        op3 = RangeTransform.new([s3[0][i]],[
            ss3[0][8*i + 0], ss3[0][8*i + 1], ss3[0][8*i + 2], ss3[0][8*i + 3], ss3[0][8*i + 4], ss3[0][8*i + 5], ss3[0][8*i + 6], ss3[0][8*i + 7]
        ])
        dag.operators.push(op0, op1, op2, op3)
    end
    ss_0,_,_ = dag.register_block(*permutation_block("P_S0_bits_#{round_number}",ss0,pLayer[0]))
    ss_1,_,_ = dag.register_block(*permutation_block("P_S1_bits_#{round_number}",ss1,pLayer[1]))
    ss_2,_,_ = dag.register_block(*permutation_block("P_S2_bits_#{round_number}",ss2,pLayer[2]))
    ss_3,_,_ = dag.register_block(*permutation_block("P_S3_bits_#{round_number}",ss3,pLayer[3]))
    ps0,_,_ = dag.register_block(*input_block("P_s0_#{round_number}",BYTE_RANGE,[1,4]))
    ps1,_,_ = dag.register_block(*input_block("P_s1_#{round_number}",BYTE_RANGE,[1,4]))
    ps2,_,_ = dag.register_block(*input_block("P_s2_#{round_number}",BYTE_RANGE,[1,4]))
    ps3,_,_ = dag.register_block(*input_block("P_s3_#{round_number}",BYTE_RANGE,[1,4]))
    4.times do |i|
        op0 = RangeTransform.new([ss_0[0][8*i + 0], ss_0[0][8*i + 1], ss_0[0][8*i + 2], ss_0[0][8*i + 3], ss_0[0][8*i + 4], ss_0[0][8*i + 5], ss_0[0][8*i + 6], ss_0[0][8*i + 7]],[
            ps0[0][i]
        ])
        op1 = RangeTransform.new([ss_1[0][8*i + 0], ss_1[0][8*i + 1], ss_1[0][8*i + 2], ss_1[0][8*i + 3], ss_1[0][8*i + 4], ss_1[0][8*i + 5], ss_1[0][8*i + 6], ss_1[0][8*i + 7]],[
            ps1[0][i]
        ])
        op2 = RangeTransform.new([ss_2[0][8*i + 0], ss_2[0][8*i + 1], ss_2[0][8*i + 2], ss_2[0][8*i + 3], ss_2[0][8*i + 4], ss_2[0][8*i + 5], ss_2[0][8*i + 6], ss_2[0][8*i + 7]],[
            ps2[0][i]
        ])
        op3 = RangeTransform.new([ss_3[0][8*i + 0], ss_3[0][8*i + 1], ss_3[0][8*i + 2], ss_3[0][8*i + 3], ss_3[0][8*i + 4], ss_3[0][8*i + 5], ss_3[0][8*i + 6], ss_3[0][8*i + 7]],[
            ps3[0][i]
        ])
        dag.operators.push(op0, op1, op2, op3)
    end
    return ps0, ps1, ps2, ps3
end
def extract_round_key(dag,key,round_number)
    round_key_1 = [4.times.map{|i| key[0][i+12]}]
    round_key_2 = [4.times.map{|i| key[0][i+ 4]}]
    round_key_3,_,_ = dag.register_block(*input_block("RC_#{round_number}_#{round_number}",BYTE_RANGE, [1,4], values=[[0x80,0x00,0x00,rc[0][round_number]]]))
    return round_key_1, round_key_2, round_key_3
end
def update_key(dag,key,round_number)
    b,_,_ = dag.register_block(*input_block("KU_#{round_number}",BOOL_RANGE,[1,128]))
    16.times do |i|
        op = RangeTransform.new([key[0][i]],[
            b[0][8*i + 0], b[0][8*i + 1], b[0][8*i + 2], b[0][8*i + 3], b[0][8*i + 4], b[0][8*i + 5], b[0][8*i + 6], b[0][8*i + 7]
        ])
        dag.operators.push(op)
    end
    b,_,_ = dag.register_block(*permutation_block("KP_#{round_number}",b,pShiftKey))
    updated_key,_,_ = dag.register_block(*input_block("K_#{round_number}",BYTE_RANGE,[1,16]))
    16.times do |i|
        op = RangeTransform.new([b[0][8*i + 0], b[0][8*i + 1], b[0][8*i + 2], b[0][8*i + 3], b[0][8*i + 4], b[0][8*i + 5], b[0][8*i + 6], b[0][8*i + 7]], [
            updated_key[0][i]
        ])
        dag.operators.push(op)
    end
    return updated_key
end
def create_gift_cofb_dag(nb_rounds)
    dag = Dag.new([],[],[],[])
    x,_,_ = dag.register_block(*input_block("X", BYTE_RANGE, [1,16]))
    k,_,_ = dag.register_block(*input_block("K", BYTE_RANGE, [1,16]))
    dag.set_inputs(x.flatten+k.flatten)
    ## Variable Set Up
    s0 = [4.times.map{|i| x[0][i+ 0]}]
    s1 = [4.times.map{|i| x[0][i+ 4]}]
    s2 = [4.times.map{|i| x[0][i+ 8]}]
    s3 = [4.times.map{|i| x[0][i+12]}]
    key = k
    nb_rounds.times do |round_number|
        #Substitution Box
        s0, s1, s2, s3 = SubsBox(nb_rounds,dag,s0,s1,s2,s3)
        #Bit permutation
        s0, s1, s2, s3 = PermBits(dag,round_number,s0,s1,s2,s3)
        # Extract round key
        round_key_1, round_key_2, round_key_3 = extract_round_key(dag,key,round_number)
        #AddRoundKey
        s1,_,_ = dag.register_block(*xor_block("ARK_s1_#{round_number}",[round_key_1,s1]))
        s2,_,_ = dag.register_block(*xor_block("ARK_s2_#{round_number}",[round_key_2,s2]))
        s3,_,_ = dag.register_block(*xor_block("ARK_s3_#{round_number}",[round_key_3,s3]))
        #KeyUpdate
        key = update_key(dag,key,round_number)
    end
    ciphertext = [s0, s1, s2, s3]
    dag.set_outputs(ciphertext.flatten)
    return dag
end