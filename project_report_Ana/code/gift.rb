#!/ruby
##
# Implements Gift 64 - 128

require_relative "../dag.rb"
require_relative "../operators.rb"
require_relative "../blocks/input.rb"
require_relative "../blocks/xor.rb"
require_relative "../blocks/permutation.rb"
require_relative "../blocks/subcell.rb"
def sbox()
    return[
        #0x1, 0xa, 0x4, 0xc, 0x6, 0xf, 0x3, 0x9, 0x2, 0xd, 0xb, 0x7, 0x5, 0x0, 0x8, 0xe
         0x8, 0x4, 0x6, 0xa, 0x2, 0xd, 0xc, 0x1, 0x5, 0xb, 0xf, 0x0, 0x3, 0xe, 0x9, 0x7
    ]
end
def pLayer()
    return[
        [
            0,  5, 10, 15, 16, 21, 26, 31, 32, 37, 42, 47, 48, 53, 58, 63,
           12,  1,  6, 11, 28, 17, 22, 27, 44, 33, 38, 43, 60, 49, 54, 59,
            8, 13,  2,  7, 24, 29, 18, 23, 40, 45, 34, 39, 56, 61, 50, 55,
            4,  9, 14,  3, 20, 25, 30, 19, 36, 41, 46, 35, 52, 57, 62, 51
        ],
        [ 
             0,  5, 10, 15,  16, 21, 26, 31,  32, 37, 42, 47,  48, 53, 58, 63,  64, 69, 74, 79,  80, 85, 90, 95,   96, 101, 106, 111,  112, 117, 122, 127,
            12,  1,  6, 11,  28, 17, 22, 27,  44, 33, 38, 43,  60, 49, 54, 59,  76, 65, 70, 75,  92, 81, 86, 91,  108,  97, 102, 107,  124, 113, 118, 123,
             8, 13,  2,  7,  24, 29, 18, 23,  40, 45, 34, 39,  56, 61, 50, 55,  72, 77, 66, 71,  88, 93, 82, 87,  104, 109,  98, 103,  120, 125, 114, 119,
             4,  9, 14,  3,  20, 25, 30, 19,  36, 41, 46, 35,  52, 57, 62, 51,  68, 73, 78, 67,  84, 89, 94, 83,  100, 105, 110,  99,  116, 121, 126, 115
        ]
    ]
end
def pRoundKey()
    return[
        [0, 16, 32, 48,   1, 17, 33, 49,   2, 18, 34, 50,   3, 19, 35, 51,
         4, 20, 36, 52,   5, 21, 37, 53,   6, 22, 38, 54,   7, 23, 39, 55,
         8, 24, 40, 56,   9, 25, 41, 57,  10, 26, 42, 58,  11, 27, 43, 59,
        12, 28, 44, 60,  13, 29, 45, 61,  14, 30, 46, 62,  15, 31, 47, 63],
        [# 0,   1,   2,   3,   4,   5,   6,   7,   8,   9,  10,  11,  12,  13,  14,  15,  16,  17,  18,  19,  20,  21,  22,  23,  24,  25,  26,  27,  28,  29,  30,  31,
         #32,  33,  34,  35,  36,  37,  38,  39,  40,  41,  42,  43,  44,  45,  46,  47,  48,  49,  50,  51,  52,  53,  54,  55,  56,  57,  58,  59,  60,  61,  62,  63,
         #64,  65,  66,  67,  68,  69,  70,  71,  72,  73,  74,  75,  76,  77,  78,  79,  80,  81,  82,  83,  84,  85,  86,  87,  88,  89,  90,  91,  92,  93,  94,  95,
         #96,  97,  98,  99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111,  112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 124, 125, 126, 127
        32,  0, 64,  96,   33,  1, 65,  97,   34,  2, 66,  98,   35,  3, 67,  99,   36,  4, 68, 100,   37,  5, 69, 101,   38,  6, 70, 102,   39,  7, 71, 103,
        40,  8, 72, 104,   41,  9, 73, 105,   42, 10, 74, 106,   43, 11, 75, 107,   44, 12, 76, 108,   45, 13, 77, 109,   46, 14, 78, 110,   47, 15, 79, 111,
        48, 16, 80, 112,   49, 17, 81, 113,   50, 18, 82, 114,   51, 19, 83, 115,   52, 20, 84, 116,   53, 21, 85, 117,   54, 22, 86, 118,   55, 23, 87, 119,
        56, 24, 88, 120,   57, 25, 89, 121,   58, 26, 90, 122,   59, 27, 91, 123,   60, 28, 92, 124,   61, 29, 93, 125,   62, 30, 94, 126,   63, 31, 95, 127]
    ]
end
def pShiftKey
    return[
         32,  33,  34,  35,  36,  37,  38,  39,  40,  41,  42,  43,  44,  45,  46,  47,
         48,  49,  50,  51,  52,  53,  54,  55,  56,  57,  58,  59,  60,  61,  62,  63,
         64,  65,  66,  67,  68,  69,  70,  71,  72,  73,  74,  75,  76,  77,  78,  79,
         80,  81,  82,  83,  84,  85,  86,  87,  88,  89,  90,  91,  92,  93,  94,  95,
         96,  97,  98,  99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111,
        112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 124, 125, 126, 127,
         12,  13,  14,  15,   0,   1,   2,   3,   4,   5,   6,   7,   8,   9,  10,  11,
         18,  19,  20,  21,  22,  23,  24,  25,  26,  27,  28,  29,  30,  31,  16,  17
    ]
end
def rc()
     return[
        [0, 0, 0, 0, 0, 1], [0, 0, 0, 0, 1, 1], [0, 0, 0, 1, 1, 1], [0, 0, 1, 1, 1, 1], [0, 1, 1, 1, 1, 1], [1, 1, 1, 1, 1, 0], [1, 1, 1, 1, 0, 1],
        [1, 1, 1, 0, 1, 1], [1, 1, 0, 1, 1, 1], [1, 0, 1, 1, 1, 1], [0, 1, 1, 1, 1, 0], [1, 1, 1, 1, 0, 0], [1, 1, 1, 0, 0, 1], [1, 1, 0, 0, 1, 1],
        [1, 0, 0, 1, 1, 1], [0, 0, 1, 1, 1, 0], [0, 1, 1, 1, 0, 1], [1, 1, 1, 0, 1, 0], [1, 1, 0, 1, 0, 1], [1, 0, 1, 0, 1, 1], [0, 1, 0, 1, 1, 0],
        [1, 0, 1, 1, 0, 0], [0, 1, 1, 0, 0, 0], [1, 1, 0, 0, 0, 0], [1, 0, 0, 0, 0, 1], [0, 0, 0, 0, 1, 0], [0, 0, 0, 1, 0, 1], [0, 0, 1, 0, 1, 1],
        [0, 1, 0, 1, 1, 1], [1, 0, 1, 1, 1, 0], [0, 1, 1, 1, 0, 0], [1, 1, 1, 0, 0, 0], [1, 1, 0, 0, 0, 1], [1, 0, 0, 0, 1, 1], [0, 0, 0, 1, 1, 0],
        [0, 0, 1, 1, 0, 1], [0, 1, 1, 0, 1, 1], [1, 1, 0, 1, 1, 0], [1, 0, 1, 1, 0, 1], [0, 1, 1, 0, 1, 0], [1, 1, 0, 1, 0, 0], [1, 0, 1, 0, 0, 1],
        [0, 1, 0, 0, 1, 0], [1, 0, 0, 1, 0, 0], [0, 0, 1, 0, 0, 0], [0, 1, 0, 0, 0, 1], [1, 0, 0, 0, 1, 0], [0, 0, 0, 1, 0, 0]
    ]
end
def create_gift_dag(nb_rounds,m_size)
    dag = Dag.new([],[],[],[])
    size = m_size/4
    x,_,_ = dag.register_block(*input_block("X",NIBBLE_RANGE, [1,size]))
    k,_,_ = dag.register_block(*input_block("K", BYTE_RANGE, [1,16]))
    dag.set_inputs(x.flatten+k.flatten)
    initial_message_state = x
    initial_key_state = k
    initial_key_state_nibble,_,_ = dag.register_block(*input_block("K_nibble",NIBBLE_RANGE, [1,32]))
    version = 3
    if m_size == 64
        version = 0
    elsif m_size == 128
        version = 1
    else
        raise "Gift message size must be 64 or 128 bits, current: #{m_size}"
    end
    16.times do |i|
        op2 = ByteToNibble.new([initial_key_state[0][i]],[
            initial_key_state_nibble[0][i*2+0], initial_key_state_nibble[0][i*2+1]
        ])
        dag.operators.push(op2)
    end
    initial_key_state_bits,_,_ = dag.register_block(*input_block("K_bits",BOOL_RANGE,[1,128]))
    32.times do |i|
        op = NibbleToBit.new([initial_key_state_nibble[0][i]],[
            initial_key_state_bits[0][4*i+0], initial_key_state_bits[0][4*i+1], initial_key_state_bits[0][4*i+2], initial_key_state_bits[0][4*i+3]
        ])
        dag.operators.push(op)
    end
    nb_rounds.times do |round_number|
        #Substitution Box
        state_sbox,_,_ = dag.register_block(*subcell_block("S_#{round_number}",initial_message_state,sbox))
        #Bit permutation
        state_sbox_bits,_,_ = dag.register_block(*input_block("S_bits_#{round_number}",BOOL_RANGE,[1,m_size]))
        size.times do |i|
            op = NibbleToBit.new([state_sbox[0][i]], [
                state_sbox_bits[0][i*4+0], state_sbox_bits[0][i*4+1], state_sbox_bits[0][i*4+2], state_sbox_bits[0][i*4+3]
            ])
            dag.operators.push(op)
        end
        state_pLayer_bits,_,_ = dag.register_block(*permutation_block("P_bits_#{round_number}",state_sbox_bits,pLayer[version]))
        #Round constant
        round_constant_bits = [m_size.times.map{|i| 0}]
        round_constant_bits[0][m_size-1] = 1
        round_constant_bits[0][23] = rc[round_number][0]
        round_constant_bits[0][19] = rc[round_number][1]
        round_constant_bits[0][15] = rc[round_number][2]
        round_constant_bits[0][11] = rc[round_number][3]
        round_constant_bits[0][7]  = rc[round_number][4]
        round_constant_bits[0][3]  = rc[round_number][5]
        state_round_constant_bits,_,_ = dag.register_block(*input_block("RC_bits_#{round_number}",BOOL_RANGE, [1,m_size], values=round_constant_bits))
        # Extract round key
        if m_size == 64
            round_key_bits = [32.times.map{|i| initial_key_state_bits[0][i]}, (32..63).map{|i| state_round_constant_bits[0][1]}]
        elsif m_size == 128
            round_key_bits = [32.times.map{|i| initial_key_state_bits[0][i]}, (32..63).map{|i| state_round_constant_bits[0][1]}, (64..95).map{|i| initial_key_state_bits[0][i]}, (96..127).map{|i| state_round_constant_bits[0][1]}]
        else
            raise "Gift message size must be 64 or 128 bits, current: #{m_size}"
        end
        round_key_bits = [round_key_bits.flatten]
        round_key_bits
        state_round_key_bits,_,_ = dag.register_block(*permutation_block("RK_#{round_number}",round_key_bits,pRoundKey[version]))
        #Add round key-constant
        state_ark_bits,_,_= dag.register_block(*xor_block("ARK_bits_#{round_number}",[state_pLayer_bits, state_round_key_bits, state_round_constant_bits]))
        state_ark,_,_ = dag.register_block(*input_block("ARK_#{round_number}",NIBBLE_RANGE, [1,size]))
        size.times do |i|
            op1 = BitToNibble.new([state_ark_bits[0][i*4+0], state_ark_bits[0][i*4+1], state_ark_bits[0][i*4+2], state_ark_bits[0][i*4+3]],[
                state_ark[0][i]
            ])
            dag.operators.push(op1)
        end
        initial_message_state = state_ark
        ##### KEY UPDATE ####
        state_key_updated_bits,_,_ = dag.register_block(*permutation_block("KP_#{round_number}",initial_key_state_bits,pShiftKey))
        initial_key_state_bits = state_key_updated_bits
    end
    dag.set_outputs(initial_message_state.flatten)
    return dag
end