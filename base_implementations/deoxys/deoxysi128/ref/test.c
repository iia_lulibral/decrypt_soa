#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <inttypes.h>
#include "tweakableBC.c"

int prt_uint8(uint8_t *array, int length){
	int i;
    for (i = 0; i < length; i++){
        printf("%x ", array[i]);
    }
    printf("\n\n");
    return 0;
}

int deoxysKeySetupEnctest(uint32_t* rtweakey,
                         const uint8_t* TweakKey,
                         const int no_tweakeys)
{

  int r;
  uint8_t tweakey[3][16];
  uint8_t alpha[3];
  const uint32_t rcon_row1 = 0x01020408;
  int Nr;

  memcpy (tweakey[0], TweakKey +  0, 16);
  memcpy (tweakey[1], TweakKey + 16, 16);

  if( 2 == no_tweakeys ){
    alpha[0] = 2;
    alpha[1] = 1;

    /* Number of rounds is 14 */
    Nr=14;

  } else if( 3 == no_tweakeys ){
    memcpy (tweakey[2], TweakKey + 32, 16);
    alpha[0] = 3;
    alpha[1] = 2;
    alpha[2] = 1;

    /* Number of rounds is 16 */
    Nr=16;

  } else {
    printf("The #tweakeys is %d, and it should be either 2 or 3. Exiting...\n", no_tweakeys);
    exit(1);
  }

  /* For each rounds */
  for(r=0; r<=Nr; r++) {

    /* Produce the round tweakey */
    rtweakey[ 4*r + 0] = GETU32( tweakey[0] +  0 ) ^ GETU32( tweakey[1] +  0 ) ^ ((3==no_tweakeys)* GETU32( tweakey[2] +  0 )) ^ rcon_row1 ;
    rtweakey[ 4*r + 1] = GETU32( tweakey[0] +  4 ) ^ GETU32( tweakey[1] +  4 ) ^ ((3==no_tweakeys)* GETU32( tweakey[2] +  4 )) ^ GETRCON( r);
    rtweakey[ 4*r + 2] = GETU32( tweakey[0] +  8 ) ^ GETU32( tweakey[1] +  8 ) ^ ((3==no_tweakeys)* GETU32( tweakey[2] +  8 ));
    rtweakey[ 4*r + 3] = GETU32( tweakey[0] + 12 ) ^ GETU32( tweakey[1] + 12 ) ^ ((3==no_tweakeys)* GETU32( tweakey[2] + 12 ));

    /* Apply H and G functions */
    if (2 == no_tweakeys) {
      H (tweakey[0]);
      G (tweakey[0], alpha[0]);
      H (tweakey[1]);
    }
    else if (3 == no_tweakeys) {
     H (tweakey[0]);
     G (tweakey[0], alpha[0]);
     H (tweakey[1]);
     G (tweakey[1], alpha[1]);
     H (tweakey[2]);
    }

  }/*r*/

  return Nr;
}
/*******************************************************************************/
void aesTweakEncryptest(uint32_t tweakey_size,
                     uint8_t pt[16],
                     uint8_t key[],
                     uint8_t ct[16]) {

    uint32_t s0, s1, s2, s3, t0, t1, t2, t3;
    uint32_t rk[4*17];
    uint8_t tmp[16];

    /* Produce the round tweakeys */
    deoxysKeySetupEnctest(rk, key, tweakey_size/128);

    /* Get the plaintext + key/tweak prewhitening */
    s0 = GETU32(pt     ) ^ rk[0];
    s1 = GETU32(pt +  4) ^ rk[1];
    s2 = GETU32(pt +  8) ^ rk[2];
    s3 = GETU32(pt + 12) ^ rk[3];
    printf("RK - 0\n");
    PUTU32(tmp     , rk[0]);
    PUTU32(tmp +  4, rk[1]);
    PUTU32(tmp +  8, rk[2]);
    PUTU32(tmp + 12, rk[3]);
    prt_uint8(tmp,16);
    printf("pt + tweakey prewhitening\n");
    PUTU32(tmp     , s0);
    PUTU32(tmp +  4, s1);
    PUTU32(tmp +  8, s2);
    PUTU32(tmp + 12, s3);
    prt_uint8(tmp,16);

    /* round 1: */
    t0 = Te0[s0 >> 24] ^ Te1[(s1 >> 16) & 0xff] ^ Te2[(s2 >>  8) & 0xff] ^ Te3[s3 & 0xff] ^ rk[ 4];
    t1 = Te0[s1 >> 24] ^ Te1[(s2 >> 16) & 0xff] ^ Te2[(s3 >>  8) & 0xff] ^ Te3[s0 & 0xff] ^ rk[ 5];
    t2 = Te0[s2 >> 24] ^ Te1[(s3 >> 16) & 0xff] ^ Te2[(s0 >>  8) & 0xff] ^ Te3[s1 & 0xff] ^ rk[ 6];
    t3 = Te0[s3 >> 24] ^ Te1[(s0 >> 16) & 0xff] ^ Te2[(s1 >>  8) & 0xff] ^ Te3[s2 & 0xff] ^ rk[ 7];
    printf("RK - 1\n");
    PUTU32(tmp     , rk[4]);
    PUTU32(tmp +  4, rk[5]);
    PUTU32(tmp +  8, rk[6]);
    PUTU32(tmp + 12, rk[7]);
    prt_uint8(tmp,16);
    printf("Round 1:\n");
    PUTU32(tmp     , t0);
    PUTU32(tmp +  4, t1);
    PUTU32(tmp +  8, t2);
    PUTU32(tmp + 12, t3);
    prt_uint8(tmp,16);
    /* round 2: */
    s0 = Te0[t0 >> 24] ^ Te1[(t1 >> 16) & 0xff] ^ Te2[(t2 >>  8) & 0xff] ^ Te3[t3 & 0xff] ^ rk[ 8];
    s1 = Te0[t1 >> 24] ^ Te1[(t2 >> 16) & 0xff] ^ Te2[(t3 >>  8) & 0xff] ^ Te3[t0 & 0xff] ^ rk[ 9];
    s2 = Te0[t2 >> 24] ^ Te1[(t3 >> 16) & 0xff] ^ Te2[(t0 >>  8) & 0xff] ^ Te3[t1 & 0xff] ^ rk[10];
    s3 = Te0[t3 >> 24] ^ Te1[(t0 >> 16) & 0xff] ^ Te2[(t1 >>  8) & 0xff] ^ Te3[t2 & 0xff] ^ rk[11];
    printf("RK - 2\n");
    PUTU32(tmp     , rk[8]);
    PUTU32(tmp +  4, rk[9]);
    PUTU32(tmp +  8, rk[10]);
    PUTU32(tmp + 12, rk[11]);
    prt_uint8(tmp,16);
    printf("Round 2:\n");
    PUTU32(tmp     , s0);
    PUTU32(tmp +  4, s1);
    PUTU32(tmp +  8, s2);
    PUTU32(tmp + 12, s3);
    prt_uint8(tmp,16);
    /* round 3: */
    t0 = Te0[s0 >> 24] ^ Te1[(s1 >> 16) & 0xff] ^ Te2[(s2 >>  8) & 0xff] ^ Te3[s3 & 0xff] ^ rk[12];
    t1 = Te0[s1 >> 24] ^ Te1[(s2 >> 16) & 0xff] ^ Te2[(s3 >>  8) & 0xff] ^ Te3[s0 & 0xff] ^ rk[13];
    t2 = Te0[s2 >> 24] ^ Te1[(s3 >> 16) & 0xff] ^ Te2[(s0 >>  8) & 0xff] ^ Te3[s1 & 0xff] ^ rk[14];
    t3 = Te0[s3 >> 24] ^ Te1[(s0 >> 16) & 0xff] ^ Te2[(s1 >>  8) & 0xff] ^ Te3[s2 & 0xff] ^ rk[15];
    printf("RK - 3\n");
    PUTU32(tmp     , rk[12]);
    PUTU32(tmp +  4, rk[13]);
    PUTU32(tmp +  8, rk[14]);
    PUTU32(tmp + 12, rk[15]);
    prt_uint8(tmp,16);
    printf("Round 3:\n");
    PUTU32(tmp     , t0);
    PUTU32(tmp +  4, t1);
    PUTU32(tmp +  8, t2);
    PUTU32(tmp + 12, t3);
    prt_uint8(tmp,16);
    /* round 4: */
    s0 = Te0[t0 >> 24] ^ Te1[(t1 >> 16) & 0xff] ^ Te2[(t2 >>  8) & 0xff] ^ Te3[t3 & 0xff] ^ rk[16];
    s1 = Te0[t1 >> 24] ^ Te1[(t2 >> 16) & 0xff] ^ Te2[(t3 >>  8) & 0xff] ^ Te3[t0 & 0xff] ^ rk[17];
    s2 = Te0[t2 >> 24] ^ Te1[(t3 >> 16) & 0xff] ^ Te2[(t0 >>  8) & 0xff] ^ Te3[t1 & 0xff] ^ rk[18];
    s3 = Te0[t3 >> 24] ^ Te1[(t0 >> 16) & 0xff] ^ Te2[(t1 >>  8) & 0xff] ^ Te3[t2 & 0xff] ^ rk[19];
    printf("RK - 4\n");
    PUTU32(tmp     , rk[16]);
    PUTU32(tmp +  4, rk[17]);
    PUTU32(tmp +  8, rk[18]);
    PUTU32(tmp + 12, rk[19]);
    prt_uint8(tmp,16);
    printf("Round 4:\n");
    PUTU32(tmp     , s0);
    PUTU32(tmp +  4, s1);
    PUTU32(tmp +  8, s2);
    PUTU32(tmp + 12, s3);
    prt_uint8(tmp,16);
    /* round 5: */
    t0 = Te0[s0 >> 24] ^ Te1[(s1 >> 16) & 0xff] ^ Te2[(s2 >>  8) & 0xff] ^ Te3[s3 & 0xff] ^ rk[20];
    t1 = Te0[s1 >> 24] ^ Te1[(s2 >> 16) & 0xff] ^ Te2[(s3 >>  8) & 0xff] ^ Te3[s0 & 0xff] ^ rk[21];
    t2 = Te0[s2 >> 24] ^ Te1[(s3 >> 16) & 0xff] ^ Te2[(s0 >>  8) & 0xff] ^ Te3[s1 & 0xff] ^ rk[22];
    t3 = Te0[s3 >> 24] ^ Te1[(s0 >> 16) & 0xff] ^ Te2[(s1 >>  8) & 0xff] ^ Te3[s2 & 0xff] ^ rk[23];
    printf("RK - 5\n");
    PUTU32(tmp     , rk[20]);
    PUTU32(tmp +  4, rk[21]);
    PUTU32(tmp +  8, rk[22]);
    PUTU32(tmp + 12, rk[23]);
    prt_uint8(tmp,16);
    printf("Round 5:\n");
    PUTU32(tmp     , t0);
    PUTU32(tmp +  4, t1);
    PUTU32(tmp +  8, t2);
    PUTU32(tmp + 12, t3);
    prt_uint8(tmp,16);
    /* round 6: */
    s0 = Te0[t0 >> 24] ^ Te1[(t1 >> 16) & 0xff] ^ Te2[(t2 >>  8) & 0xff] ^ Te3[t3 & 0xff] ^ rk[24];
    s1 = Te0[t1 >> 24] ^ Te1[(t2 >> 16) & 0xff] ^ Te2[(t3 >>  8) & 0xff] ^ Te3[t0 & 0xff] ^ rk[25];
    s2 = Te0[t2 >> 24] ^ Te1[(t3 >> 16) & 0xff] ^ Te2[(t0 >>  8) & 0xff] ^ Te3[t1 & 0xff] ^ rk[26];
    s3 = Te0[t3 >> 24] ^ Te1[(t0 >> 16) & 0xff] ^ Te2[(t1 >>  8) & 0xff] ^ Te3[t2 & 0xff] ^ rk[27];
    printf("RK - 6\n");
    PUTU32(tmp     , rk[24]);
    PUTU32(tmp +  4, rk[25]);
    PUTU32(tmp +  8, rk[26]);
    PUTU32(tmp + 12, rk[27]);
    prt_uint8(tmp,16);
    printf("Round 6:\n");
    PUTU32(tmp     , s0);
    PUTU32(tmp +  4, s1);
    PUTU32(tmp +  8, s2);
    PUTU32(tmp + 12, s3);
    prt_uint8(tmp,16);
    /* round 7: */
    t0 = Te0[s0 >> 24] ^ Te1[(s1 >> 16) & 0xff] ^ Te2[(s2 >>  8) & 0xff] ^ Te3[s3 & 0xff] ^ rk[28];
    t1 = Te0[s1 >> 24] ^ Te1[(s2 >> 16) & 0xff] ^ Te2[(s3 >>  8) & 0xff] ^ Te3[s0 & 0xff] ^ rk[29];
    t2 = Te0[s2 >> 24] ^ Te1[(s3 >> 16) & 0xff] ^ Te2[(s0 >>  8) & 0xff] ^ Te3[s1 & 0xff] ^ rk[30];
    t3 = Te0[s3 >> 24] ^ Te1[(s0 >> 16) & 0xff] ^ Te2[(s1 >>  8) & 0xff] ^ Te3[s2 & 0xff] ^ rk[31];
    printf("RK - 7\n");
    PUTU32(tmp     , rk[28]);
    PUTU32(tmp +  4, rk[29]);
    PUTU32(tmp +  8, rk[30]);
    PUTU32(tmp + 12, rk[31]);
    prt_uint8(tmp,16);
    printf("Round 7:\n");
    PUTU32(tmp     , t0);
    PUTU32(tmp +  4, t1);
    PUTU32(tmp +  8, t2);
    PUTU32(tmp + 12, t3);
    prt_uint8(tmp,16);
    /* round 8: */
    s0 = Te0[t0 >> 24] ^ Te1[(t1 >> 16) & 0xff] ^ Te2[(t2 >>  8) & 0xff] ^ Te3[t3 & 0xff] ^ rk[32];
    s1 = Te0[t1 >> 24] ^ Te1[(t2 >> 16) & 0xff] ^ Te2[(t3 >>  8) & 0xff] ^ Te3[t0 & 0xff] ^ rk[33];
    s2 = Te0[t2 >> 24] ^ Te1[(t3 >> 16) & 0xff] ^ Te2[(t0 >>  8) & 0xff] ^ Te3[t1 & 0xff] ^ rk[34];
    s3 = Te0[t3 >> 24] ^ Te1[(t0 >> 16) & 0xff] ^ Te2[(t1 >>  8) & 0xff] ^ Te3[t2 & 0xff] ^ rk[35];
    printf("RK - 8\n");
    PUTU32(tmp     , rk[32]);
    PUTU32(tmp +  4, rk[33]);
    PUTU32(tmp +  8, rk[34]);
    PUTU32(tmp + 12, rk[35]);
    prt_uint8(tmp,16);
    printf("Round 8:\n");
    PUTU32(tmp     , s0);
    PUTU32(tmp +  4, s1);
    PUTU32(tmp +  8, s2);
    PUTU32(tmp + 12, s3);
    prt_uint8(tmp,16);
    /* round 9: */
    t0 = Te0[s0 >> 24] ^ Te1[(s1 >> 16) & 0xff] ^ Te2[(s2 >>  8) & 0xff] ^ Te3[s3 & 0xff] ^ rk[36];
    t1 = Te0[s1 >> 24] ^ Te1[(s2 >> 16) & 0xff] ^ Te2[(s3 >>  8) & 0xff] ^ Te3[s0 & 0xff] ^ rk[37];
    t2 = Te0[s2 >> 24] ^ Te1[(s3 >> 16) & 0xff] ^ Te2[(s0 >>  8) & 0xff] ^ Te3[s1 & 0xff] ^ rk[38];
    t3 = Te0[s3 >> 24] ^ Te1[(s0 >> 16) & 0xff] ^ Te2[(s1 >>  8) & 0xff] ^ Te3[s2 & 0xff] ^ rk[39];
    printf("RK - 9\n");
    PUTU32(tmp     , rk[36]);
    PUTU32(tmp +  4, rk[37]);
    PUTU32(tmp +  8, rk[38]);
    PUTU32(tmp + 12, rk[39]);
    prt_uint8(tmp,16);
    printf("Round 9:\n");
    PUTU32(tmp     , t0);
    PUTU32(tmp +  4, t1);
    PUTU32(tmp +  8, t2);
    PUTU32(tmp + 12, t3);
    prt_uint8(tmp,16);
    /* round 10: */
    s0 = Te0[t0 >> 24] ^ Te1[(t1 >> 16) & 0xff] ^ Te2[(t2 >>  8) & 0xff] ^ Te3[t3 & 0xff] ^ rk[40];
    s1 = Te0[t1 >> 24] ^ Te1[(t2 >> 16) & 0xff] ^ Te2[(t3 >>  8) & 0xff] ^ Te3[t0 & 0xff] ^ rk[41];
    s2 = Te0[t2 >> 24] ^ Te1[(t3 >> 16) & 0xff] ^ Te2[(t0 >>  8) & 0xff] ^ Te3[t1 & 0xff] ^ rk[42];
    s3 = Te0[t3 >> 24] ^ Te1[(t0 >> 16) & 0xff] ^ Te2[(t1 >>  8) & 0xff] ^ Te3[t2 & 0xff] ^ rk[43];
    printf("RK - 10\n");
    PUTU32(tmp     , rk[40]);
    PUTU32(tmp +  4, rk[41]);
    PUTU32(tmp +  8, rk[42]);
    PUTU32(tmp + 12, rk[43]);
    prt_uint8(tmp,16);
    printf("Round 10:\n");
    PUTU32(tmp     , s0);
    PUTU32(tmp +  4, s1);
    PUTU32(tmp +  8, s2);
    PUTU32(tmp + 12, s3);
    prt_uint8(tmp,16);
    /* round 11: */
    t0 = Te0[s0 >> 24] ^ Te1[(s1 >> 16) & 0xff] ^ Te2[(s2 >>  8) & 0xff] ^ Te3[s3 & 0xff] ^ rk[44];
    t1 = Te0[s1 >> 24] ^ Te1[(s2 >> 16) & 0xff] ^ Te2[(s3 >>  8) & 0xff] ^ Te3[s0 & 0xff] ^ rk[45];
    t2 = Te0[s2 >> 24] ^ Te1[(s3 >> 16) & 0xff] ^ Te2[(s0 >>  8) & 0xff] ^ Te3[s1 & 0xff] ^ rk[46];
    t3 = Te0[s3 >> 24] ^ Te1[(s0 >> 16) & 0xff] ^ Te2[(s1 >>  8) & 0xff] ^ Te3[s2 & 0xff] ^ rk[47];
    printf("RK - 11\n");
    PUTU32(tmp     , rk[44]);
    PUTU32(tmp +  4, rk[45]);
    PUTU32(tmp +  8, rk[46]);
    PUTU32(tmp + 12, rk[47]);
    prt_uint8(tmp,16);
    printf("Round 11:\n");
    PUTU32(tmp     , t0);
    PUTU32(tmp +  4, t1);
    PUTU32(tmp +  8, t2);
    PUTU32(tmp + 12, t3);
    prt_uint8(tmp,16);
    /* round 12: */
    s0 = Te0[t0 >> 24] ^ Te1[(t1 >> 16) & 0xff] ^ Te2[(t2 >>  8) & 0xff] ^ Te3[t3 & 0xff] ^ rk[48];
    s1 = Te0[t1 >> 24] ^ Te1[(t2 >> 16) & 0xff] ^ Te2[(t3 >>  8) & 0xff] ^ Te3[t0 & 0xff] ^ rk[49];
    s2 = Te0[t2 >> 24] ^ Te1[(t3 >> 16) & 0xff] ^ Te2[(t0 >>  8) & 0xff] ^ Te3[t1 & 0xff] ^ rk[50];
    s3 = Te0[t3 >> 24] ^ Te1[(t0 >> 16) & 0xff] ^ Te2[(t1 >>  8) & 0xff] ^ Te3[t2 & 0xff] ^ rk[51];
    printf("RK - 12\n");
    PUTU32(tmp     , rk[48]);
    PUTU32(tmp +  4, rk[49]);
    PUTU32(tmp +  8, rk[50]);
    PUTU32(tmp + 12, rk[51]);
    prt_uint8(tmp,16);
    printf("Round 12:\n");
    PUTU32(tmp     , s0);
    PUTU32(tmp +  4, s1);
    PUTU32(tmp +  8, s2);
    PUTU32(tmp + 12, s3);
    prt_uint8(tmp,16);
    /* round 13: */
    t0 = Te0[s0 >> 24] ^ Te1[(s1 >> 16) & 0xff] ^ Te2[(s2 >>  8) & 0xff] ^ Te3[s3 & 0xff] ^ rk[52];
    t1 = Te0[s1 >> 24] ^ Te1[(s2 >> 16) & 0xff] ^ Te2[(s3 >>  8) & 0xff] ^ Te3[s0 & 0xff] ^ rk[53];
    t2 = Te0[s2 >> 24] ^ Te1[(s3 >> 16) & 0xff] ^ Te2[(s0 >>  8) & 0xff] ^ Te3[s1 & 0xff] ^ rk[54];
    t3 = Te0[s3 >> 24] ^ Te1[(s0 >> 16) & 0xff] ^ Te2[(s1 >>  8) & 0xff] ^ Te3[s2 & 0xff] ^ rk[55];
    printf("RK - 13\n");
    PUTU32(tmp     , rk[52]);
    PUTU32(tmp +  4, rk[53]);
    PUTU32(tmp +  8, rk[54]);
    PUTU32(tmp + 12, rk[55]);
    prt_uint8(tmp,16);
    printf("Round 13:\n");
    PUTU32(tmp     , t0);
    PUTU32(tmp +  4, t1);
    PUTU32(tmp +  8, t2);
    PUTU32(tmp + 12, t3);
    prt_uint8(tmp,16);
    /* round 14: */
    s0 = Te0[t0 >> 24] ^ Te1[(t1 >> 16) & 0xff] ^ Te2[(t2 >>  8) & 0xff] ^ Te3[t3 & 0xff] ^ rk[56];
    s1 = Te0[t1 >> 24] ^ Te1[(t2 >> 16) & 0xff] ^ Te2[(t3 >>  8) & 0xff] ^ Te3[t0 & 0xff] ^ rk[57];
    s2 = Te0[t2 >> 24] ^ Te1[(t3 >> 16) & 0xff] ^ Te2[(t0 >>  8) & 0xff] ^ Te3[t1 & 0xff] ^ rk[58];
    s3 = Te0[t3 >> 24] ^ Te1[(t0 >> 16) & 0xff] ^ Te2[(t1 >>  8) & 0xff] ^ Te3[t2 & 0xff] ^ rk[59];
    printf("RK - 14\n");
    PUTU32(tmp     , rk[56]);
    PUTU32(tmp +  4, rk[57]);
    PUTU32(tmp +  8, rk[58]);
    PUTU32(tmp + 12, rk[59]);
    prt_uint8(tmp,16);
    printf("Round 14:\n");
    PUTU32(tmp     , s0);
    PUTU32(tmp +  4, s1);
    PUTU32(tmp +  8, s2);
    PUTU32(tmp + 12, s3);
    prt_uint8(tmp,16);

    if (384 == tweakey_size) {
      /* round 15: */
      t0 = Te0[s0 >> 24] ^ Te1[(s1 >> 16) & 0xff] ^ Te2[(s2 >>  8) & 0xff] ^ Te3[s3 & 0xff] ^ rk[60];
      t1 = Te0[s1 >> 24] ^ Te1[(s2 >> 16) & 0xff] ^ Te2[(s3 >>  8) & 0xff] ^ Te3[s0 & 0xff] ^ rk[61];
      t2 = Te0[s2 >> 24] ^ Te1[(s3 >> 16) & 0xff] ^ Te2[(s0 >>  8) & 0xff] ^ Te3[s1 & 0xff] ^ rk[62];
      t3 = Te0[s3 >> 24] ^ Te1[(s0 >> 16) & 0xff] ^ Te2[(s1 >>  8) & 0xff] ^ Te3[s2 & 0xff] ^ rk[63];
      printf("RK - 15\n");
      PUTU32(tmp     , rk[60]);
      PUTU32(tmp +  4, rk[61]);
      PUTU32(tmp +  8, rk[62]);
      PUTU32(tmp + 12, rk[63]);
      prt_uint8(tmp,16);
      printf("Round 15:\n");
      PUTU32(tmp     , t0);
      PUTU32(tmp +  4, t1);
      PUTU32(tmp +  8, t2);
      PUTU32(tmp + 12, t3);
      prt_uint8(tmp,16);
      /* round 16: */
      s0 = Te0[t0 >> 24] ^ Te1[(t1 >> 16) & 0xff] ^ Te2[(t2 >>  8) & 0xff] ^ Te3[t3 & 0xff] ^ rk[64];
      s1 = Te0[t1 >> 24] ^ Te1[(t2 >> 16) & 0xff] ^ Te2[(t3 >>  8) & 0xff] ^ Te3[t0 & 0xff] ^ rk[65];
      s2 = Te0[t2 >> 24] ^ Te1[(t3 >> 16) & 0xff] ^ Te2[(t0 >>  8) & 0xff] ^ Te3[t1 & 0xff] ^ rk[66];
      s3 = Te0[t3 >> 24] ^ Te1[(t0 >> 16) & 0xff] ^ Te2[(t1 >>  8) & 0xff] ^ Te3[t2 & 0xff] ^ rk[67];
      printf("RK - 16\n");
      PUTU32(tmp     , rk[64]);
      PUTU32(tmp +  4, rk[65]);
      PUTU32(tmp +  8, rk[66]);
      PUTU32(tmp + 12, rk[67]);
      prt_uint8(tmp,16);
      printf("Round 16:\n");
      PUTU32(tmp     , s0);
      PUTU32(tmp +  4, s1);
      PUTU32(tmp +  8, s2);
      PUTU32(tmp + 12, s3);
      prt_uint8(tmp,16);
    }

    /* Put the state into the ciphertext */
    PUTU32(ct     , s0);
    PUTU32(ct +  4, s1);
    PUTU32(ct +  8, s2);
    PUTU32(ct + 12, s3);
    prt_uint8(ct,16);
}

int main(){
  uint8_t pt[16] = {
    0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00
  };
  uint8_t tweakey1[32] = {
    0x10,0x11,0x12,0x13,
    0x14,0x15,0x16,0x17,
    0x18,0x19,0x1a,0x1b,
    0x1c,0x1d,0x1e,0x1f,
    0x12,0x02,0x12,0x22,
    0x32,0x42,0x52,0x62,
    0x70,0x00,0x00,0x00,
    0x00,0x00,0x00,0x00
  };
    uint8_t tweakey2[32] = {
    0x00,0x04,0x08,0x0c,
    0x01,0x05,0x09,0x0d,
    0x02,0x06,0x0a,0x0e,
    0x03,0x07,0x0b,0x0f,
    0x00,0x00,0x00,0x00,
    0x00,0x00,0x00,0x00,
    0x00,0x00,0x00,0x00,
    0x00,0x00,0x00,0x00
  };
  uint8_t tweakey3[32] = {
    0x00,0x00,0x00,0x00,
    0x00,0x00,0x00,0x00,
    0x00,0x00,0x00,0x00,
    0x00,0x00,0x00,0x00,
    0x00,0x04,0x08,0x0c,
    0x01,0x05,0x09,0x0d,
    0x02,0x06,0x0a,0x0e,
    0x03,0x07,0x0b,0x0f
  };
  uint8_t tweakey4[32] = {
    0x00,0x00,0x00,0x00,
    0x00,0x00,0x00,0x00,
    0x00,0x00,0x00,0x00,
    0x00,0x00,0x00,0x00,
    0x00,0x00,0x00,0x00,
    0x00,0x00,0x00,0x00,
    0x00,0x00,0x00,0x00,
    0x00,0x00,0x00,0x00
  };
  uint8_t ct1[16] = {
    0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00
  };
  uint8_t ct2[16] = {
    0xee,0xc8,0x7d,0xce,
    0x98,0xd2,0x9d,0x40,
    0x78,0x59,0x8a,0xbd,
    0x16,0xd5,0x50,0xff
  };
  printf("primera llamada\n"); /*21 7c 32 30 36 6a 60 6c 32 3c 04 1a 3a 36 4c 40*/
  aesTweakEncryptest(0x100,pt,tweakey1,ct1);
  printf("segunda llamada\n"); /*09 10 18 0e 54 4a 40 5e 0c 16 18 02 0e 10 1a 04 */
  aesTweakEncryptest(0x100,pt,tweakey2,ct1);
  printf("tercera llamada\n"); /*05 0b 0a 0b 5b 54 51 5e 06 0b 0c 01 07 08 0d 02 */
  aesTweakEncryptest(0x100,pt,tweakey3,ct1);
  printf("cuarta llamada\n"); /*1 2 4 8 5e 5e 5e 5e 0 0 0 0 0 0 0 0 */
  aesTweakEncryptest(0x100,pt,tweakey4,ct1);
  return 0;
}