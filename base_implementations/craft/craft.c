#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>

const int S[16] =
{12, 10, 13, 3, 14, 11, 15, 7, 8, 9, 1, 5, 0, 2, 4, 6};

const int P[16] =
{15, 12, 13, 14, 10, 9, 8, 11, 6, 5, 4, 7, 1, 2, 3, 0};

const int Q[16] =
{12, 10, 15, 5, 14, 8, 9, 2, 11, 3, 7, 4, 6, 0, 1, 13};

const int RC3 [32] =
{
	1, 4, 2, 5, 6, 7, 3, 1, 4, 2, 5, 6, 7, 3, 1, 4,
	2, 5, 6, 7, 3, 1, 4, 2, 5, 6, 7, 3, 1, 4, 2, 5 
};

const int RC4 [32] =
{
	1, 8, 4, 2, 9, 12, 6, 11, 5, 10, 13, 14, 15, 7, 3, 1, 
	8, 4, 2, 9, 12, 6, 11, 5, 10, 13, 14, 15, 7, 3, 1, 8 
};

// int Key [2][16] =
// {
// 	{2, 7, 10, 6, 7, 8, 1, 10, 4, 3, 15, 3, 6, 4, 11, 12},
// 	{9, 1, 6, 7, 0, 8, 13, 5, 15, 11, 11, 5, 10, 14, 15, 14} 
// };

// int Tweak [16] = 
// {5, 4, 12, 13, 9, 4, 15, 15, 13, 0, 6, 7, 0, 10, 5, 8};

// int Tweak2[16];

// int Stt [16] = 
// {5, 7, 3, 4, 15, 0, 0, 6, 13, 8, 13, 8, 8, 10, 3, 14};

int Key [2][16] =
{
    {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},
    {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}
};

int Tweak [16] =
{0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};

int Tweak2[16];

int Stt [16] =
{0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};

int TK[4][16];

void printTK(int (*tab)[16])
{
	for(int i = 0; i < 4; i++)
	{
		printf("TK%d : ",i );
        for(int j = 0; j < 16; j++)
        {
            printf("%d ",tab[i][j]);
        }
        printf("\n");
    }
    printf("\n");
}

void printP(const int * tab)
{
	printf("P : ");
	for (int i = 0; i < 16; i++)
	{
		printf("%d ",tab[i]);
	}
	printf("\n");
	printf("\n");
}

void printS(const int * tab)
{
	printf("S : ");
	for (int i = 0; i < 16; i++)
	{
		printf("%d ",tab[i] );
	}
	printf("\n");
	printf("\n");
}

void printTK_i(int (*tab)[16], int i)
{
	printf("TK%d : ",i );
    for(int j = 0; j < 16; j++)
    {
        printf("%d ",tab[i][j]);
    }
    printf("\n");
}
void printKey(int (*tab)[16])
{
	for(int i = 0; i < 2; i++)
	{
		printf("Key%d : ",i );
        for(int j = 0; j < 16; j++)
        {
            printf("%d ",tab[i][j]);
        }
        printf("\n");
    }
    printf("\n");
}

void printKey_i(int (*tab)[16], int i)
{
	printf("Key%d : ",i );
    for(int j = 0; j < 16; j++)
    {
        printf("%d ",tab[i][j]);
    }
    printf("\n");
}

void printTweak(int * tab )
{
	printf("TWEAK : ");
	for (int i = 0; i < 16; i++)
	{
		printf("%d ",tab[i] );
	}
	printf("\n");
}

void printTemp(int * tab)
{
	printf("Temp : ");
	for (int i = 0; i < 16; i++)
	{
		printf("%d ",tab[i] );
	}
	printf("\n");
	printf("\n");
}

void printStt(int * tab)
{
	printf("Stt : ");
	for (int i = 0; i < 16; i++)
	{
		printf("%d ",tab[i] );
	}
	printf("\n");
	printf("\n");
}

void PrintTweak2(int * tab)
{
	printf("TWEAK 2 : ");
	for (int i = 0; i < 16; i++)
	{
		printf("%d ",tab[i] );
	}
	printf("\n");
}

void printCipher(int * tab, int r)
{
	if (r != 31)
	{
		printf("C%d : ", r+1);
		for (int i = 0; i < 16; i++)
		{
			printf("%d ",tab[i] );
		}
		printf("\n");
	}
	else
	{
		printf("\n");
		printf("Cipher : ");
		for (int i = 0; i < 16; i++)
		{
			printf("%d ",tab[i] );
		}
		printf("\n");
	}
}

void printPlainText(int * tab, int r)
{
	if (r != 31)
	{
		printf("Plain%d : ", r+1);
		for (int i = 0; i < 16; i++)
		{
			printf("%d ",tab[i] );
		}
		printf("\n");
	}
	else
	{
		printf("\n");
		printf("PlainText : ");
		for (int i = 0; i < 16; i++)
		{
			printf("%d ",tab[i] );
		}
		printf("\n");
	}
}

void printRC4(const int * tab)
{
	printf("RC4 : ");
	for (int i = 0; i < 16; i++)
	{
		printf("%d ",tab[i] );
	}
	printf("\n");
}

void printRC3(const int * tab)
{
	printf("RC3 : ");
	for (int i = 0; i < 16; i++)
	{
		printf("%d ",tab[i] );
	}
	printf("\n");
}

void Initialize_key (int dec) 
{

	//Initialize Tweak2
	for (int i = 0; i < 16; i ++)
	{
		Tweak2[i] = Tweak[ Q[i]];
	}

	//Display variables
	printTK(TK);
	printKey(Key);
	printStt(Stt);

	if (!dec)
	{
		//Display how we create TKs
		printf("                          INITIALIZING TK1, TK2, TK3 et TK4\n\n");

		//TK1
		printKey_i(Key, 0);
		printTweak(Tweak);
		printf("\n");
		for (int i = 0; i < 16; i ++)
		{
			printf("TK0[%d] = Key0[%d] XOR Tweak[%d] = %d XOR %d = %d \n",i,i,i,Key[0][i],Tweak[i], Key[0][i] ^ Tweak[i]);
		}
		printf("\n\n");
		
		//TK2
		printKey_i(Key, 1);
		printTweak(Tweak);
		printf("\n");
		for (int i = 0; i < 16; i ++)
		{
			printf("TK1[%d] = Key1[%d] XOR Tweak[%d] = %d XOR %d = %d \n",i,i,i,Key[1][i],Tweak[i], Key[1][i] ^ Tweak[i]);
		}
		printf("\n\n");
		
		//TK3
		printKey_i(Key, 0);
		PrintTweak2(Tweak2);
		printf("\n");
		for (int i = 0; i < 16; i ++)
		{
			printf("TK2[%d] = Key0[%d] XOR Tweak2[%d] = %d XOR %d = %d \n",i,i,i,Key[0][i],Tweak2[i], Key[0][i] ^ Tweak2[i]);
		}
		printf("\n\n");
		
		//TK4
		printKey_i(Key, 1);
		PrintTweak2(Tweak2);
		printf("\n");
		for (int i = 0; i < 16; i ++)
		{
			printf("TK3[%d] = Key1[%d] XOR Tweak2[%d] = %d XOR %d = %d \n",i,i,i,Key[1][i],Tweak2[i], Key[1][i] ^ Tweak2[i]);
		}
		printf("\n\n");
	}

	//Create TK1, TK2, TK3, TK4
	for (int i = 0; i < 16; i ++)
	{
		TK[0][i] = Key[0][i] ^ Tweak[i];
		TK[1][i] = Key[1][i] ^ Tweak[i];
		TK[2][i] = Key[0][i] ^ Tweak2[i];
		TK[3][i] = Key[1][i] ^ Tweak2[i];
	}

	if (!dec)
	{
		printTK(TK);
	}

	if (dec) 
	{
		printf("\nDecryption phase\n\n");
		// Display Operations
		for ( int j = 0; j < 4; j++ )
		{
			printf("\n");
			for (int i = 0; i < 4; i++)
			{
				printf("TK[%d][%d] = TK[%d][%d] XOR (TK[%d][%d] XOR TK[%d][%d]) = %d XOR (%d XOR %d) = %d \n", j, i, j, i, j, i+8, j, i+12, TK[j][i], TK[j][i+8], TK[j][i+12], TK[j][i] ^ (TK[j][i + 8] ^ TK[j][i + 12]) );
			}
			for (int i = 0; i < 4; i++)
			{
				printf("TK[%d][%d] = TK[%d][%d] XOR TK[%d][%d] = %d XOR %d = %d \n", j, i+4, j, i+4, j, i+12, TK[j][i+4], TK[j][i+12], TK [j][i+4] ^ TK [j][i + 12]); 
			}
			printf("\n");
		}
		// Operations
		for ( int j = 0; j < 4; j++ )
		{
			for (int i = 0; i < 4; i++)
			{
				TK[j][i] ^= ( TK[j][i + 8] ^ TK[j][i + 12] );
				TK[j][i + 4] ^= TK[j][i + 12]; 
			}
		}

		//TKs
		printTK(TK);
	}
}

 void Round (int r, int dec)
{	

//Mix columns
	//Display first round of encryption
	if (r == 0 && !dec)
	{
		printf("ROUND %d \n\n",r+1);
		printf("		MIX COLUMNS\n");
		printStt(Stt);

		//Display operations
		for (int i = 0; i < 4; i++)
		{
			printf("Stt[%d] = Stt[%d] XOR Stt[%d] XOR Stt[%d] =  %d XOR %d XOR %d = %d \n",i,i,i+8,i+12,Stt[i],Stt[i+8],Stt[i+12], Stt [i] ^ ( Stt [i + 8] ^ Stt [i + 12] ));
		}
		printf("\n");

		for (int i = 0; i < 4; i++)
		{
			printf("Stt[%d] = Stt[%d] XOR Stt[%d] = %d XOR %d = %d\n",i+4,i+4,i+12, Stt[i+4], Stt[i+12], Stt [i + 4] ^ Stt [i + 12]);
		}
		printf("\n");
	}

	//Opérations
	for (int i = 0; i < 4; i++)
	{
		Stt [i] ^= ( Stt [i + 8] ^ Stt [i + 12] );
		Stt [i + 4] ^= Stt [i + 12];
	}

	if (r == 0 && !dec)
	{
		printStt(Stt);
	}

	int ind = r;
	if (dec)
	{
		ind = 31 - r;
	}

//Add Constant
	if (r == 0 && !dec)
	{
		printf("		ADD CONSTANT\n\n");
		printStt(Stt);
		printRC3(RC3);
		printRC4(RC4);
		printf("r = %d\n\n",r );

		//Display operations
		printf("Stt[4] = Stt[4] XOR RC4[r] = %d XOR %d = %d\n", Stt[4], RC4[r], Stt[4] ^ RC4[r]);
		printf("Stt[5] = Stt[5] XOR RC3[r] = %d XOR %d = %d\n\n", Stt[5], RC3[r], Stt[5] ^ RC3[r]);
	}

	//Operations
	Stt [4] ^= RC4 [ind];
	Stt [5] ^= RC3 [ind];

	if (r == 0 && !dec)
	{
		printStt(Stt);
	}

//Add Tweakey
	if (r == 0 && !dec)
	{
		printf("		ADD TWEAKEY\n\n");
		printStt(Stt);
		printTK(TK);

		//Display operations
		for (int i = 0; i < 16; i++)
		{
			printf("Stt[%d] = Stt[%d] XOR TK[%d%%4][%d] = %d XOR %d = %d\n",i,i,r,i,Stt[i],TK[r%4][i],Stt[i] ^ TK[r%4][i]);
		}
		printf("\n\n");
	}

	//Operations
	for (int i = 0; i < 16; i++)
	{
		Stt [i] ^= TK [ind % 4][i];
	}

	if (r == 0 && !dec)
	{
		printStt(Stt);
	}

	//If it's not the last round
	if (r != 31)
	{
		//Create Temp
		int Temp [16] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

//Permutation
		if (r == 0 && !dec)
		{
			printf("		PERMUTATION\n\n");
			printStt(Stt);
			printTemp(Temp);
			printP(P);

			//Display operations
			for (int i = 0; i < 16; i++)
			{
				printf("P[%d] = %d --> Temp[%d] = Stt[%d] = %d\n",i, P[i],P[i],i, Stt[i]);
			}
			printf("\n\n");
		}

		//Opérations
		for (int i = 0; i < 16; i++)
		{
			Temp [ P[i] ] = Stt [i];
		}

		if (r == 0 && !dec)
		{
			printTemp(Temp);
		}

//Sbox
		if (r == 0 && !dec)
		{
			printf("		SBOX\n\n");
			printStt(Stt);
			printTemp(Temp);
			printS(S);

			//Display operations
			for (int i = 0; i < 16; i++)
			{
				printf("Temp[%d] = %d, S[%d] = %d --> Stt[%d] = %d \n",i, Temp[i], Temp[i], S[Temp[i]], i, S[Temp[i]] );
			}
			printf("\n\n");
		}

		//Operations
		for (int i = 0; i < 16; i++)
		{
			Stt [ i ] = S [ Temp[i] ];
		}

		if (r == 0 && !dec)
		{
			printStt(Stt);
			printf("-----------------------------------------------------------------------\n");
		}
	}

	//Print Cipher at the end of each round
	if (dec)
	{
		printPlainText(Stt, r);
	}

	else
	{
		printCipher(Stt, r);
	}
}

int main (int argc, char * argv [])
{
	int dec = 0; 
	printf("Encryption of the PlainText ");
	printStt(Stt);

	Initialize_key(dec);

	for (int r = 0; r < 32; r++)
	{
		Round(r, dec);
	}

	// printf("\n\n\n\n");

	// dec = 1;
	// printf("Decryption of the Cipher ");
	// printStt(Stt);
	
	// Initialize_key(dec);

	// for (int r = 0; r < 32; r++)
	// {
	// 	Round(r, dec);
	// }
	
	return 0;
}


		
