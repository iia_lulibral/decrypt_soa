/*
==============================================================================================================
TODO : 
changer unsigned par unsigned char 
==============================================================================================================
*/

#include<stdio.h>
#include<stdlib.h>
#include<string.h>

#define BLOCK_SIZE  128
#define KEY_SIZE 128
#define ROUNDS 32

//--simple display to make sure functions are working fine during coding
void display_tab(unsigned *tab, int taille){
	for(int i=0; i<taille; i++)
	{
		printf("%x ", tab[i]);
	}
	printf("\n");
}


void init_LFSR(unsigned* lfsr){
	lfsr[0] = 0b1011010;
	for(int i=1;; i++){
		unsigned new_c = lfsr[i -1];
		new_c = (unsigned)((((new_c << 3) ^ (new_c << 6)) & 0x40)|(new_c >> 1)); 
		lfsr[i] = new_c;
		if(new_c == lfsr[0]){
			break;
		}
	}
}

void whitening_key_generation(unsigned* mk, unsigned* wk){
	for(int i=0; i<8; i++)
	{
		if(i >= 0 && i<=3)
		{
			wk[i] = mk[i+12];
		} else
		{
			wk[i] = mk[i-4];
		}
	}
}

void subkeys_generation(unsigned* mk, unsigned* lfsr, unsigned* sk){
	for(int i=0; i<8; i++)
	{
		for(int j=0; j<8; j++)
		{
			sk[16 * i + j] = (unsigned)((mk[(j-i) & 0x7] + lfsr[16 * i + j]) & 0xff);
		}
		for(int j = 0; j<8; j++)
		{
			sk[16*i+j] = (unsigned)((mk[(j-i) & 0x7] + lfsr[16 * i + j]) & 0xff);
			sk[16*i+j+8] = (unsigned)((mk[((j-i) & 0x7) + 8] + lfsr[16 * i + j + 8]) & 0xff);
		}
	}
}



void initial_transformation(unsigned* text, unsigned* whitened, unsigned* wk){
	whitened[7] = (unsigned)((text[0] + wk[0]) & 0xff);
	whitened[6] = text[1];
	whitened[5] = (unsigned)((text[2] ^ wk[1]) & 0xff);
	whitened[4] = text[3];
	whitened[3] = (unsigned)((text[4] + wk[2]) & 0xff);
	whitened[2] = text[5];
	whitened[1] = (unsigned)((text[6] ^ wk[3]) & 0xff);
	whitened[0] = text[7];
}

unsigned part1_round_function(unsigned text){
	unsigned op1 = (unsigned)((text << 1)|(text >> 7)); 
	unsigned op2 = (unsigned)((text << 2)|(text >> 6)); 
	unsigned op3 = (unsigned)((text << 3)|(text >> 1)); 
	return (unsigned)(op1^op2^op3);
}

unsigned part2_round_function(unsigned text){
	unsigned op1 = (unsigned)((text << 3)|(text >> 5));
	unsigned op2 = (unsigned)((text << 4)|(text >> 4));
	unsigned op3 = (unsigned)((text << 5)|(text >> 2));
	return (unsigned)(op1^op2^op3);
}


void perform_rounds(unsigned* text, unsigned* new_text, unsigned* sk){
	for(int i = 0; i < ROUNDS -1; i++)
	{
		new_text[6]=text[7];
		new_text[4]=text[5];
		new_text[2]=text[3];
		new_text[0]=text[1];

		new_text[7] = (unsigned)(text[0] ^ ((part1_round_function(text[1]) + sk[4*i+3]) & 0xff));
		new_text[5] = (unsigned)((text[6] + (part2_round_function(text[7]) ^ sk[4*i])) & 0xff);
		new_text[3] = (unsigned)(text[4] ^ ((part1_round_function(text[5]) + sk[4*i+1]) & 0xff));
		new_text[1] = (unsigned)((text[2] + (part2_round_function(text[3]) ^ sk[4*i+2])) & 0xff);
		printf("Round %d : ", i);
		display_tab(new_text,8);
		text = new_text;
	}
} 


int main(int argc, char** argv){
//--variables declaration
    unsigned mk[] = {0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xaa, 0xbb, 0xcc, 0xdd, 0xee, 0xff};
	// unsigned mk[] = {68,57,73,59,71,33,74,36,77,39,80,24,43,26,46,29}; //master key
	// unsigned plaintext[] = {1,2,3,4,5,6,7,8};
    unsigned plaintext[] = {0,0,0,0,0,0,0,0};
	unsigned wk[8]; 		// whitening keys 
	unsigned sk[128]; 		// subkeys  	
	unsigned whitened[8]; 	// for initial transformation
	unsigned final_cipher_text[8]; 	// for round function
	unsigned lfsr[128]; 	// stock the 128 constants for subkeys generation

	// key shedule
	whitening_key_generation(mk,wk);
	init_LFSR(lfsr);
	subkeys_generation(mk,lfsr,sk);
	printf("master key : "); display_tab(mk,16);
	printf("\n");
	printf("plaintext : "); display_tab(plaintext,8);
	printf("\n");
	printf("whitening keys  : "); display_tab(wk,8);
	printf("\n");
	printf("constants  : "); display_tab(lfsr,128);
	printf("\n");
	printf("subkeys : "); display_tab(sk,128);
	printf("\n");
	initial_transformation(plaintext,whitened,wk);
	printf("text after initial transformation : "); display_tab(whitened,8);
	printf("\n");
	perform_rounds(whitened, final_cipher_text,sk);
	printf("\n");
	printf("cipher text 1 : ");display_tab(final_cipher_text,8);printf("\n");
	
	return 0;
}